## Documentação Arena Torcedor  

Os dados que serão fornecidos são de total responsabilidad da OptaPerform.  
Caso exista um erro de dado, verificar com a Perform qual erro referente.  

Para simplificar e ficar bem objetivo a documentação, será utilizado a variável de documentação, a variável sempre irá iniciar com $. 

$uri = `Referente a URi que é utilizado na para a requisição (arenatorcedor.com.br), (local.arenatorcedor.com.br)`  
$acceptOnlyJson = `{ Accept: application/json }  `  
$1 = `Primeiro tempo`  
$2 = `Segundo tempo`  
$3 = `Terceiro tempo ( porrogação )`
  
## Utilidades  
**Data**  
Header: **$acceptOnlyJson**  
Method: GET  
URi: **$uri**/api/date  
Descrição do retorno: Data atual do servidor, facilitando o uso de data para o client-side.  
  
**URi's disponíveis**  
Header: **$acceptOnlyJson**  
Method: GET  
URi: **$uri**/api/base-uri  
Descrição do retorno: Todas as URi's disponíveis para facilitar o uso.    
  
## Partidas e Campeonatos  
    
**Listagem dos times**  
Header: **$acceptOnlyJson**  
Method: GET  
URi: **$uri**/api/teams  
Descrição do retorno: Todos os times cadastrados até o momento.
  
**Campeonatos**  
Header: **$acceptOnlyJson**  
Method: GET  
URi: **$uri**/api/competitions  
Descrição do retorno: Todos os campeonatos até o momento (alguns campeonatos podem não estar habilitados para o contrato jurídico).  
  
**Classificações do campeonato**  
Header: **$acceptOnlyJson**  
Method: GET  
URi: **$uri**/api/{código da competição}/rank  
Descrição do retorno: Classificação dos times referente ao campeonato.    
  
**Times do campeonato**  
Header: **$acceptOnlyJson**  
Method: GET  
URi: **$uri**/api/{código da competição}/teams  
Descrição do retorno: Todos os times referentes ao campeonato.  

**Times por Grupo**  
Header: **$acceptOnlyJson**  
Method: GET  
URi: **$uri**/api/{código da competição}/groups  
Descrição do retorno: Lista os times por GRUPO  

**Partidas do Campeonato**  
Header: **$acceptOnlyJson**  
Method: GET  
URi: **$uri**/api/{código da competição}/matches  
Descrição do retorno: Lista todas as partidas referente ao campeonato  
  
**Dados da Partida**  
Header: **$acceptOnlyJson**  
Method: GET  
URi: **$uri**/api/{código da partida}/match  
Descrição do retorno: Dados referente a partida atual  
  
**Partida atual e futuras (Paginação)**  
Header: **$acceptOnlyJson**  
Method: GET  
URi: **$uri**/api/{código da competição}/paginate-next-match  
Descrição do retorno: Vai ser paginado as partidas do dia e as futuras partidas ( de acordo com o campeonato )  
Parâmetros extras: ?page=(int) número da página
  
**Partida atual e antigas (Paginação)**  
Header: **$acceptOnlyJson**  
Method: GET  
URi: **$uri**/api/{código da competição}/paginate-previous-match  
Descrição do retorno: Paginação de todas as partidas anteriores ao dia atual  
Parâmetros extras: ?page=(int) número da página  
  
**Próxima partida (Paginação)**  
Header: **$acceptOnlyJson**  
Method: GET  
URi: **$uri**/api/{código da competição}/next-matches  
Descrição do retorno: Trás apenas uma partida referente a data e hora atual em diante
Parâmetros extras: ?page=(int) número da página  
  
**Voltar partida (Paginação)**  
Header: **$acceptOnlyJson**  
Method: GET  
URi: **$uri**/api/{código da competição}/previous-matches  
Descrição do retorno: Todas as partidas anteriores a data e hora atual  
Parâmetros extras: ?page=(int) número da página  
  
**Partidas do dia (Paginação)**  
Header: **$acceptOnlyJson**  
Method: GET  
URi: **$uri**/api/{código da competição}/{dia (dd)}/{mês (mm)}/{ano (YYYY)}/matches  
Descrição do retorno: Paginação correspondente a data fornecida  
Parâmetros extras: ?page=(int) número da página  
  
## HeatMAP  
**Partida**  
Header: **$acceptOnlyJson**  
Method: GET  
URi: **$uri**/api/{código da partida}/heatmap/{periodo}  
Descrição do retorno: Mapa de calor da partida podendo ser visualizada por tempo   
Parâmetros extras /{periodo} não obrigatório, caso informado apenas ($1, $2 ou $3)  
  
**Jogador**  
Header: **$acceptOnlyJson**  
Method: GET  
URi: **$uri**/api/{código da partida}/player/{código do jogador}/heatmap/{periodo}  
Descrição do retorno: Mapa de calor do jogador na partida, podendo ser visualizada por tempo    
Parâmetros extras /{periodo} não obrigatório, caso informado apenas ($1, $2 ou $3)  
  
**Time**  
Header: **$acceptOnlyJson**  
Method: GET  
URi: **$uri**/api/{código da partida}/team/{código do time}/heatmap/{periodo}  
Descrição do retorno: Mapa de calor do time na partida, podendo ser utilizado por tempo  
Parâmetros extras /{periodo} não obrigatório, caso informado apenas ($1, $2 ou $3)  
  
## Informações  
**Jogador**  
Header: **$acceptOnlyJson**  
Method: GET  
URi: **$uri**/api/{código do jogador}/about  
Descrição do retorno: Retorna alguns dados referente ao jogador  
    
  
