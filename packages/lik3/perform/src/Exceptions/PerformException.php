<?php

namespace Lik3\Perform\Exceptions;

use Exception;

/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 28/03/2018
 * Class PerformException
 * @package Lik3\Perform\Exceptions
 */
class PerformException extends Exception
{

}