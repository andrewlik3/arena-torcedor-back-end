<?php
/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 05/04/2018
 */

namespace Lik3\Perform\Commands;

use Illuminate\Console\Command;
use Lik3\Perform\Perform;

class Import extends Command
{

    /**
     * @var string
     */
    protected $signature = "perform {--storage=} {--file=} {--only=}";

    protected $description = "Importação da OptaPerform; Atributos * Não obrigatório * (php artisan perform nome do arquivo)";

    private $Perform = Perform::class;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $perform = app($this->Perform)->setCommand($this->output);

        if ($this->option('storage'))
            $perform->setStorage(storage_path($this->option('storage')));
        else
            $perform->setStorage(storage_path("opta"));

        if ($this->option('file')) {

            $this->output->text("Arquivo {$this->option('file')} está sendo lido, aguarde...");

            $perform->setFile($this->option('file'))
                ->run();

        } else {

            if ($this->option('only')) {
                $perform->specific($this->option('only'));
            } else {
                $perform->automatic();
            }
        }
    }

}