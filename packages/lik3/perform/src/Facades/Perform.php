<?php

namespace Lik3\Perform\Facades;

use Illuminate\Support\Facades\Facade;

class Perform extends Facade
{
    public static function getFacadeAccessor()
    {
        return 'Perform';
    }
}