<?php
/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 29/03/2018
 *
 * Referente aos comentários do jogo
 */

namespace Lik3\Perform\Feeds;


use App\Comment;
use App\CommentMessage;
use Carbon\Carbon;
use Lik3\Perform\Contracts\PerformInterface;
use Lik3\Perform\Exceptions\PerformException;

class F13 extends Base implements PerformInterface
{

    public $Comment;

    public function fileConvention(string $file_name)
    {
        return $this;
    }

    /**
     * @param array $data
     * @throws PerformException
     */
    public function saveComment(array $data)
    {
        $validator = validator($data, ['@competition_id' => 'required', '@away_team_id' => 'required', '@home_team_id' => 'required']);
        if (!$validator->fails()) {
            $exists = app(Comment::class)->all();
            $exist = $exists->where('game_id', $data['@game_id'])->first();
            if (!$exist) {
                $this->Comment = app(Comment::class)->create([
                    'home_team_id' => $data['@home_team_id'],
                    'away_team_id' => $data['@away_team_id'],
                    'competition_id' => $data['@competition_id'],
                    'game_id' => $data['@game_id'],
                    'season_id' => $data['@season_id'],
                    'match_day' => $data['@matchday'],
                    'match_date' => londonToBr($data['@game_date'])
                ]);
            } else {
                $this->Comment = $exist;
            }
            if (isset($data['message'])) {
                $this->saveMessages($data['message']);
            }
        } else {
            throw new PerformException($validator->fails());
        }
    }

    /**
     * @param array $data
     * @return $this
     */
    public function saveMessages(array $data)
    {
        if ($this->Comment instanceof Comment) {
            $exists = app(CommentMessage::class)->get(['code']);
            if (!isset($data[1]))
                $data = [$data];

            foreach ($data as $datum) {
                $exist = $exists->where('code', $datum['@id'])->first();
                if (!$exist) {
                    app(CommentMessage::class)->create([
                        'game_id' => $this->Comment->game_id,
                        'code' => $datum['@id'],
                        'message' => $datum['@comment'],
                        'minute' => isset($datum['@minute']) ? $datum['@minute'] : null,
                        'period' => isset($datum['@period']) ? $datum['@period'] : null,
                        'second' => isset($datum['@second']) ? $datum['@second'] : null,
                        'time' => isset($datum['@time']) ? $datum['@time'] : null,
                        'type' => isset($datum['@type']) ? $datum['@type'] : null,
                        'modified' => londonToBr($datum['@last_modified'])
                    ]);
                }
            }
        }
        return $this;
    }

    /**
     * @param $payload
     * @return mixed|void
     * @throws PerformException
     */
    public function parse($payload)
    {
        $this->saveComment($payload);
    }

}