<?php
/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 29/03/2018
 *
 * Detalhe da posição do time, tabelas, ligas e etc...
 */

namespace Lik3\Perform\Feeds;

use Lik3\Perform\Contracts\PerformInterface;
use Lik3\Perform\Exceptions\PerformException;

class F3 extends Base implements PerformInterface
{
    /**
     * @param string $file_name
     * @return $this|mixed
     */
    public function fileConvention(string $file_name)
    {
        return $this;
    }

    /**
     * @param $payload
     * @return mixed|void
     * @throws PerformException
     */
    public function parse($payload)
    {
        $this->setPayload($payload)
            ->setSoccerDocument($this->payload->get('SoccerDocument'));

        $this->round = $this->soccerDocument->get('@CurrentRound');

        $this->saveSeason([
            'id' => $this->soccerDocument->get('@season_id'),
            'name' => $this->soccerDocument->get('@season_name')
        ])->saveCompetition([
            'code' => only_numbers($this->soccerDocument->get('@competition_id')),
            'name' => $this->soccerDocument->get('@competition_name'),
            'slug_code' => $this->soccerDocument->get('@competition_code')
        ]);

        if ($this->soccerDocument->get("Team"))
            $this->saveTeams($this->soccerDocument->get('Team'));

        $Competition = $this->soccerDocument->get('Competition');
        if (isset($Competition['TeamStandings']))
            $this->saveStandings($Competition['TeamStandings']);
    }

}