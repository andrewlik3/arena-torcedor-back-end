<?php
/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 29/03/2018
 */

namespace Lik3\Perform\Feeds;

use App\Competition;
use App\Game;
use App\GamePlayerBook;
use App\GamePlayerLine;
use App\GamePlayerStat;
use App\Result;
use App\ResultMatchInfo;
use App\ResultStat;
use App\ResultTeamData;
use App\ResultTeamDataGoal;
use App\Season;
use App\Substitution;
use Lik3\Perform\Contracts\PerformInterface;
use Lik3\Perform\Exceptions\PerformException;

class F9 extends Base implements PerformInterface
{

    private $competition_id;

    private $season_id;

    private $game_id;

    private $Result;

    private $AwayTeam;

    private $HomeTeam;

    /**
     * @param string $file_name
     * @return $this|mixed
     * @throws PerformException
     *
     * srml-{competition_id}-{season_id}-f{game_id}-matchresults.xml
     */
    public function fileConvention(string $file_name)
    {
        preg_match('/srml-(?<competition_id>[0-9]+)-(?<season_id>[0-9]+)-f(?<game_id>[0-9]+)-matchresults.xml/', $file_name, $return);
        $validator = validator($return, ["competition_id" => "required", "season_id" => "required", "game_id" => "required"]);
        if (!$validator->fails()) {

            $this->competition_id = only_numbers($return["competition_id"]);
            $this->season_id = only_numbers($return["season_id"]);
            $this->game_id = only_numbers($return["game_id"]);

        }
        return $this;
    }

    /**
     * @param array $game
     * @return $this
     * @throws PerformException
     */
    private function saveGame(array $game)
    {
        if ($this->Competition instanceof Competition && $this->Season instanceof Season) {

            $validator = validator($game, [
                "MatchData" => "required",
                "@uID" => "required"
            ]);

            if (!$validator->fails()) {

                $TeamData = collect($game["MatchData"]["TeamData"]);

                $this->AwayTeam = $TeamData->where("@Side", "Away")->first();
                $this->HomeTeam = $TeamData->where("@Side", "Home")->first();

                $this->Game = app(Game::class)->createOrReturn([
                    "code" => only_numbers($game["@uID"]),
                    "type" => $game['@Type'],
                    "competition_id" => $this->Competition->code,
                    "season_id" => $this->Season->code,
                    "away_team_id" => only_numbers($this->AwayTeam["@TeamRef"]),
                    "home_team_id" => only_numbers($this->HomeTeam["@TeamRef"])
                ], "code");

                foreach ($TeamData as $teamDatum) {
                    if (isset($teamDatum["PlayerLineUp"])) {
                        $this->savePlayerLines($teamDatum["PlayerLineUp"]["MatchPlayer"]);
                    }

                    if (isset($teamDatum['Booking'])) {
                        $this->saveBooking($teamDatum['Booking']);
                    }
                }

            }
        } else {
            throw new PerformException("Competição ou temporada não foram informadas.");
        }
        return $this;
    }

    private function saveBooking($booking)
    {
        if (!isset($booking[1])) {
            $booking = [$booking];
        }

        foreach ($booking as $item) {
            app(GamePlayerBook::class)->createOrReturn([
                'code' => $item['@uID'],
                'player_id' => only_numbers($item['@PlayerRef']),
                'card_type' => $item['@CardType'],
                'reason' => $item['@Reason'],
                'period' => $item['@Period'],
                'min' => $item['@Min'],
                'sec' => $item['@Sec'],
                'time' => $item['@Time']
            ], 'code');
        }
    }

    /**
     * @param $game_player_id
     * @param array $stats
     * @return $this
     */
    private function savePlayerLineStat($game_player_id, array $stats)
    {
        if ($this->Game instanceof Game) {
            $player_stats = app(GamePlayerStat::class)->where("game_id", "=", $this->Game->code)
                ->where("game_player_line_id", $game_player_id)
                ->get();
            foreach ($stats as $stat) {
                if (isset($stat["@Type"])) {
                    $exist = $player_stats->where("key", $stat["@Type"])->first();
                    if (!$exist) {
                        app(GamePlayerStat::class)->create([
                            "game_id" => $this->Game->code,
                            "game_player_line_id" => $game_player_id,
                            "key" => $stat["@Type"],
                            "value" => $stat["#text"]
                        ]);
                    } else {
                        $exist->update([
                            "value" => $stat["#text"]
                        ]);
                    }
                }
            }
        }
        return $this;
    }

    /**
     * @param array $players
     * @return $this
     * @throws PerformException
     */
    private function savePlayerLines(array $players)
    {
        if ($this->Game instanceof Game) {

            foreach ($players as $player) {

                $player_id = only_numbers($player['@PlayerRef']);

                $player_inline = app(GamePlayerLine::class)->where([
                    "game_id" => $this->Game->code,
                    "player_id" => only_numbers($player_id)
                ])->first();

                if (!$player_inline) {
                    $player_inline = app(GamePlayerLine::class)->create([
                        "game_id" => $this->Game->code,
                        "player_id" => $player_id,
                        "shirt_number" => $player['@ShirtNumber'],
                        "position" => $player['@Position'],
                        "status" => $player['@Status']
                    ]);
                } else {
                    $player_inline->update([
                        "status" => $player["@Status"],
                        "position" => $player["@Position"]
                    ]);
                }

                if (isset($player['Stat'])) {
                    $this->savePlayerLineStat($player_inline->player_id, $player['Stat']);
                }

            }

        } else {
            throw new PerformException("O jogo não foi informado.");
        }
        return $this;
    }

    /**
     * @param array $f9
     * @return $this
     * @throws PerformException
     */
    private function saveResult(array $f9)
    {
        if ($this->Game instanceof Game) {

            $MatchData = $f9["MatchData"];
            $PreviousMatch = isset($MatchData["PreviousMatch"]) ? only_numbers($MatchData["PreviousMatch"]["@MatchRef"]) : 0;
            $Venue = $f9["Venue"];

            $this->saveStadium(only_numbers($this->HomeTeam['@TeamRef']), [
                "code" => only_numbers($Venue["@uID"]),
                "capacity" => 1,
                "name" => $Venue["Name"]
            ]);

            $this->Result = app(Result::class)->createOrReturn([
                "parent_id" => $PreviousMatch,
                "code" => only_numbers($f9["@uID"]),
                "game_id" => $this->Game->id,
                "stadium_id" => $this->Stadium->code,
                "status" => $f9["@Type"],
                "status_match" => $MatchData["MatchInfo"]["@Period"]
            ], "code");

            if ($this->Result) {
                $this->Result->update([
                    "status" => $f9["@Type"],
                    "status_match" => $MatchData["MatchInfo"]["@Period"]
                ]);
            }

            if (isset($MatchData['MatchInfo'])) {
                $this->saveResultMatchInfo($MatchData['MatchInfo']);
            }

            if (isset($MatchData['Stat'])) {
                $this->saveResultStat($MatchData['Stat']);
            }

            if (isset($MatchData['TeamData'])) {
                foreach ($MatchData['TeamData'] as $teamDatum) {
                    $this->saveResultTeamData($teamDatum);
                }
            }

        } else {
            throw new PerformException("Game não instânciado.");
        }
        return $this;
    }

    /**
     * @param array $teamData
     * @return $this
     */
    private function saveResultTeamData(array $teamData)
    {
        if ($this->Result instanceof Result) {

            $ResultCode = $this->Result->code;
            $team_id = only_numbers($teamData["@TeamRef"]);

            $exist = app(ResultTeamData::class)->where([
                "result_id" => $ResultCode,
                "team_id" => only_numbers($teamData["@TeamRef"])
            ])->first();

            if (!$exist) {
                $exist = app(ResultTeamData::class)->create([
                    "result_id" => $ResultCode,
                    "team_id" => $team_id,
                    "score" => isset($teamData["@Score"]) ? $teamData["@Score"] : 0,
                    "shoot_out_score" => isset($teamData["@ShootOutScore"]) ? $teamData["@ShootOutScore"] : 0,
                    "side" => $teamData["@Side"]
                ]);
            } else {
                $exist->update([
                    "score" => isset($teamData["@Score"]) ? $teamData["@Score"] : 0,
                    "shoot_out_score" => isset($teamData["@ShootOutScore"]) ? $teamData["@ShootOutScore"] : 0,
                ]);
            }

            if (isset($teamData['Goal'])) {

                $Goals = $teamData['Goal'];
                if (isset($Goals[1])) {
                    foreach ($Goals as $goal) {
                        $this->saveResultTeamDataGoal($exist->id, $goal);
                    }
                } else {
                    $this->saveResultTeamDataGoal($exist->id, $Goals);
                }

            }

            if (isset($teamData['Substitution'])) {
                $Substitution = $teamData['Substitution'];
                if (isset($Substitution[1])) {
                    foreach ($Substitution as $item) {
                        $this->saveSubstitution($item);
                    }
                } else {
                    $this->saveSubstitution($Substitution);
                }
            }

        }
        return $this;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function saveSubstitution(array $data)
    {
        if ($this->Game instanceof Game) {
            app(Substitution::class)->createOrReturn([
                "game_id" => $this->Game->code,
                "code" => $data['@uID'],
                "player_on" => only_numbers($data['@SubOn']),
                "player_off" => only_numbers($data['@SubOff']),
                "reason" => $data['@Reason'],
                "period" => $data['@Period'],
                "minute" => $data['@Min'],
                "second" => $data['@Sec']
            ], "code");
        }
        return $this;
    }

    /**
     * @param $result_team_id
     * @param array $data
     * @return $this
     */
    private function saveResultTeamDataGoal($result_team_id, array $data)
    {
        $exist = app(ResultTeamDataGoal::class)->where("code", $data["@uID"])->count();
        if (!$exist) {
            app(ResultTeamDataGoal::class)->create([
                "event_id" => $data["@EventID"],
                "team_data_id" => $result_team_id,
                "event_number" => $data["@EventNumber"],
                "type" => $data["@Type"],
                "code" => $data["@uID"],
                "minute" => $data["@Min"],
                "second" => $data["@Sec"],
                "player_id" => only_numbers($data["@PlayerRef"]),
                "player_assist_id" => isset($data["Assist"]) ? only_numbers($data["Assist"]["@PlayerRef"]) : null
            ]);
        }
        return $this;
    }

    /**
     * @param array $stats
     * @return $this
     */
    private function saveResultStat(array $stats)
    {
        if ($this->Result instanceof Result) {
            if (!isset($stats[1]))
                $stats = [$stats];

            foreach ($stats as $stat) {
                app(ResultStat::class)->firstOrCreate([
                    "result_id" => $this->Result->code,
                    "type" => $stat['@Type'],
                    "text" => $stat['#text']
                ]);
            }
        }
        return $this;
    }

    /**
     * @param array $matchInfo
     * @return $this
     */
    private function saveResultMatchInfo(array $matchInfo)
    {
        if ($this->Result instanceof Result) {

            $ResultCode = $this->Result->code;
            $winner_id = isset($matchInfo["Result"]["@Winner"]) ? only_numbers($matchInfo["Result"]["@Winner"]) : null;

            $exist = app(ResultMatchInfo::class)->where([
                "result_id" => $ResultCode
            ])->first();

            if (!$exist) {

                app(ResultMatchInfo::class)->create([
                    "period" => $matchInfo["@Period"],
                    "match_type" => $matchInfo["@MatchType"],
                    "result_type" => isset($matchInfo["@MatchType"]) ? $matchInfo["@MatchType"] : $matchInfo["Result"]["@Type"],
                    "winner_id" => $winner_id,
                    "result_id" => $ResultCode
                ]);

            } else {
                $exist->update([
                    "period" => $matchInfo["@Period"],
                    "match_type" => isset($matchInfo["@MatchType"]) ? $matchInfo["@MatchType"] : $matchInfo["Result"]["@Type"],
                    "result_type" => isset($matchInfo["Result"]) ? $matchInfo["Result"]["@Type"] : null,
                    "winner_id" => $winner_id
                ]);
            }

        }
        return $this;
    }

    /**
     * @param $payload
     * @return mixed|void
     * @throws \Lik3\Perform\Exceptions\PerformException
     */
    public function parse($payload)
    {
        $this->setPayload($payload)
            ->setSoccerDocument($this->payload->get('SoccerDocument'));

        $Check = $this->soccerDocument->toArray();

        if (isset($Check[1])) {

            foreach ($this->soccerDocument->all() as $item) {

                $this->getSeason($this->season_id);

                $this->saveCompetition([
                    "code" => only_numbers($item["Competition"]["@uID"]),
                    "name" => $item["Competition"]["Name"]
                ])->saveGame($item)
                    ->saveResult($item);
            }

        } else {
            $item = $this->soccerDocument->toArray();

            if ($this->season_id) {
                $this->getSeason($this->season_id);
            } else {

                $Competition = $this->soccerDocument->get('Competition');

                $Stat = collect($Competition['Stat']);

                $Season = $Stat->where('@Type', 'season_id')->first();

                $this->getSeason($Season['#text']);
            }

            $this->saveCompetition([
                "code" => only_numbers($item["Competition"]["@uID"]),
                "name" => $item["Competition"]["Name"]
            ])->saveGame($item)
                ->saveTeams($item['Team'])
                ->saveResult($item);
        }

    }

}