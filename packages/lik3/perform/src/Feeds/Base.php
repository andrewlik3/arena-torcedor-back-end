<?php
/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 29/03/2018
 */

namespace Lik3\Perform\Feeds;


use App\Competition;
use App\CompetitionTeam;
use App\Game;
use App\Match;
use App\MatchOfficial;
use App\Official;
use App\Player;
use App\PlayerStat;
use App\Season;
use App\Team;
use App\TeamKit;
use App\TeamOfficial;
use App\TeamPlace;
use App\TeamRecord;
use Illuminate\Support\Facades\Storage;
use Lik3\Perform\Exceptions\PerformException;

class Base
{

    public $Season;
    public $Stadium;
    public $Game;

    public $round;

    public $soccerDocument;
    public $payload;

    public $Competition;
    public $Teams;

    public $Standings = [];

    public $MatchData = [];

    public $Officials = [];

    public $Command;

    public $Handle;


    /**
     * @param $command
     * @return $this
     */
    public function setCommand($command)
    {
        $this->Command = $command;
        return $this;
    }

    /**
     * @param array $standings
     * @return $this
     */
    public function saveStandings(array $standings)
    {
        if ($this->Competition instanceof Competition) {
            foreach ($standings as $standing) {
                if (isset($standing['TeamRecord']))
                    $this->saveTeamRecords($standing['TeamRecord']);
                else {
                    if (isset($standings['TeamRecord'])) {
                        $this->saveTeamRecords($standings['TeamRecord']);
                    }
                }
            }
        }
        return $this;
    }

    /**
     * @param array $teamRecords
     */
    public function saveTeamRecords(array $teamRecords)
    {
        if ($this->Competition instanceof Competition) {
            $exists = app(TeamRecord::class)->get(['competition_id', 'team_id', 'current_round', 'standing']);
            foreach ($teamRecords as $teamRecord) {

                $team_uID = only_numbers($teamRecord['@TeamRef']);
                $exist = $exists->where('team_id', $team_uID)
                    ->where('competition_id', $this->Competition->code)
                    ->where('current_round', $this->round)->first();

                $standing = $teamRecord['Standing'];
                $gc = $standing['Against'];
                $gp = $standing['For'];
                $sg = (int)$gp - $gc;

                if (!$exist) {

                    $this->Standings[] = app(TeamRecord::class)->create([
                        'team_id' => $team_uID,
                        'competition_id' => $this->Competition->code,
                        'current_round' => $this->round,
                        'points' => $standing['Points'],
                        'drawn' => $standing['Drawn'],
                        'loose' => $standing['Lost'],
                        'win' => $standing['Won'],
                        'gc' => $gc,
                        'gp' => $gp,
                        'sg' => $sg,
                        'position' => $standing['Position'],
                        'start_day_position' => $standing['StartDayPosition'] != 'N/A' ? $standing['StartDayPosition'] : 0,
                        'standing' => $teamRecord['Standing']
                    ]);

                } else {

                    $standingBase64 = base64_encode(json_encode($exist->standing));
                    $currentBase64 = base64_encode(json_encode($teamRecord['Standing']));

                    if ($standingBase64 != $currentBase64) {
                        $this->Standings[] = $exist->update([
                            'standing' => $teamRecord['Standing'],
                            'points' => $standing['Points'],
                            'position' => $standing['Position'],
                            'start_day_position' => $standing['StartDayPosition']
                        ]);
                    }

                }
            }
        }
    }

    /**
     * @param $payload
     * @return $this|mixed
     */
    public function setPayload($payload)
    {
        $this->payload = collect($payload);
        return $this;
    }

    /**
     * @param array $payload
     * @return $this
     * @throws PerformException
     */
    public function setSoccerDocument($payload)
    {
        if (!$payload)
            throw new PerformException('SoccerDocument not found.');
        $this->soccerDocument = collect($payload);
        return $this;
    }

    /**
     * @param int $id
     * @return mixed
     * @throws PerformException
     */
    public function getSeason(int $id)
    {
        $this->Season = app(Season::class)->whereCode($id)->first();
        if (!$this->Season)
            $this->saveSeason(['id' => $id, 'name' => $id]);
        return $this->Season;
    }

    /**
     * @param array $data
     * @return $this
     * @throws PerformException
     */
    public function saveSeason(array $data)
    {
        $validator = validator($data, ['id' => 'required', 'name' => 'required']);
        if (!$validator->fails())
            $this->Season = app(Season::class)->createOrReturn($data + ['code' => $data['id']], 'code');
        else
            throw new PerformException($validator->fails());
        return $this;
    }

    /**
     * @param array $data
     * @return $this
     * @throws PerformException
     */
    public function saveCompetition(array $data)
    {
        if ($this->Season instanceof Season) {

            $validator = validator($data, ['code' => 'required', 'name' => 'required']);
            if (!$validator->fails())
                $this->Competition = app(Competition::class)->createOrReturn($data + ['season_id' => $this->Season->code], 'code');
            else
                throw new PerformException($validator->failed());

        } else {
            throw new PerformException('É preciso fornecer a temporada da competição para salvar os dados.');
        }
        return $this;
    }

    /**
     * @param array $teams
     * @param string $group_name
     * @return $this
     * @throws PerformException
     */
    public function saveTeams(array $teams)
    {
        if ($this->Competition instanceof Competition AND $this->Season instanceof Season) {
            foreach ($teams as $team) {
                $validator = validator($team, ['@uID' => 'required', 'Name' => 'required']);
                if (!$validator->fails()) {

                    $team_uID = only_numbers($team['@uID']);
                    $Team = $this->Teams[] = app(Team::class)->createOrReturn([
                        'code' => $team_uID,
                        'name' => $team['Name'],
                        'slug' => str_slug($team['Name'])
                    ], 'code');

                    if (isset($team['Player'])) {
                        if (isset($team['Player'][1])) {
                            foreach ($team['Player'] as $player) {

                                if (isset($player['Name'])) {
                                    $Name = $player['Name'];
                                } else {
                                    if (!isset($player['PersonName']['Known'])) {
                                        $Name = implode(' ', $player['PersonName']);
                                    } else {
                                        $Name = $player['PersonName']['Known'];
                                    }
                                }

                                if (!isset($player['Position']))
                                    $Position = $player['@Position'];
                                else {
                                    $Position = $player['Position'];
                                }

                                $this->savePlayer($Team->code, [
                                    'id' => $player['@uID'],
                                    'name' => $Name,
                                    'position' => $Position,
                                    'stat' => isset($player['Stat']) ? $player['Stat'] : null
                                ]);
                            }
                        } else {

                            $player = $team['Player'];

                            if (isset($player['Name'])) {
                                $Name = $player['Name'];
                            } else {
                                if (!isset($player['PersonName']['Known'])) {
                                    $Name = implode(' ', $player['PersonName']);
                                } else {
                                    $Name = $player['PersonName']['Known'];
                                }
                            }

                            if (!isset($player['Position']))
                                $Position = $player['@Position'];
                            else {
                                $Position = $player['Position'];
                            }

                            $this->savePlayer($Team->code, [
                                'id' => $player['@uID'],
                                'name' => $Name,
                                'position' => $Position,
                                'stat' => isset($player['Stat']) ? $player['Stat'] : null
                            ]);
                        }
                    }

                    # estádio do time
                    if (isset($team['Stadium'])) {
                        $stadium = $team['Stadium'];

                        if (isset($stadium['@uID']) && $stadium['@uID'] != '') {
                            $this->saveStadium($Team->code, [
                                'code' => $stadium['@uID'],
                                'capacity' => isset($stadium['Capacity']) ? $stadium['Capacity'] : 0,
                                'name' => $stadium['Name']
                            ]);
                        }

                    }

                    # Roupas disponíveis para o time
                    if (isset($team['TeamKits'])) {

                        if (isset($team['TeamKits']['Kit'][1])) {
                            foreach ($team['TeamKits']['Kit'] as $kit) {

                                if (isset($kit['@colour1']))
                                    $store['color_one'] = $kit['@colour1'];
                                if (isset($kit['@colour2']))
                                    $store['color_two'] = $kit['@colour2'];
                                if (isset($kit['@colour3']))
                                    $store['color_tree'] = $kit['@colour3'];

                                $store['type'] = $kit['@type'];
                                $store['code'] = $kit['@id'];

                                $this->saveKits($Team->code, $store);
                            }
                        } else {

                            $kit = $team['TeamKits']['Kit'];

                            if (isset($kit['@colour1']))
                                $store['color_one'] = $kit['@colour1'];
                            if (isset($kit['@colour2']))
                                $store['color_two'] = $kit['@colour2'];
                            if (isset($kit['@colour3']))
                                $store['color_tree'] = $kit['@colour3'];

                            $store['type'] = $kit['@type'];
                            $store['code'] = $kit['@id'];

                            $this->saveKits($Team->code, $store);

                        }
                    }

                    # Técnico e assistentes
                    if (isset($team['TeamOfficial'])) {
                        if (!isset($team['TeamOfficial']['@uID'])) {
                            foreach ($team['TeamOfficial'] as $teamOfficial) {
                                $this->saveTeamOfficial($Team->code, [
                                    'code' => only_numbers($teamOfficial['@uID']),
                                    'name' => $teamOfficial['PersonName']['First'],
                                    'type' => $teamOfficial['@Type'],
                                    'birth_date' => isset($teamOfficial['PersonName']['BirthDate']) && $teamOfficial['PersonName']['BirthDate'] != '0000-00-00' ? $teamOfficial['PersonName']['BirthDate'] : null,
                                    'country' => isset($teamOfficial['@country']) ? $teamOfficial['@country'] : null,
                                    'last_name' => $teamOfficial['PersonName']['Last']
                                ]);
                            }
                        }
                    }
                }
            }
        } else {
            throw new PerformException('Verifique se a competição ou temporada foram inseridas corretamente.');
        }
        return $this;
    }

    /**
     * @param $team_id
     * @param array $official
     */
    public function saveTeamOfficial($team_id, array $official)
    {
        $validator = validator($official, ['code' => 'required', 'name' => 'required', 'type' => 'required']);
        if (!$validator->fails()) {
            app(TeamOfficial::class)->createOrReturn($official + ['team_id' => $team_id], 'code');
        }
    }

    /**
     * @param $team_id
     * @param array $kits
     */
    public function saveKits($team_id, array $kits)
    {
        $validator = validator($kits, ['type' => 'required', 'code' => 'required']);
        if (!$validator->fails())
            app(TeamKit::class)->createOrReturn($kits + ['team_id' => $team_id], 'code');
    }

    /**
     * @param $team_id
     * @param array $stadium
     */
    public function saveStadium($team_id, array $stadium)
    {
        $validator = validator($stadium, [
            'code' => 'required',
            'capacity' => 'required',
            'name' => 'required'
        ]);

        if (!$validator->fails()) {
            $this->Stadium = app(TeamPlace::class)->createOrReturn($stadium + ['team_id' => $team_id], 'code');
        }
    }

    /**
     * @param $team_id
     * @param array $player
     */
    public function savePlayer($team_id, array $player)
    {
        $validator = validator($player, [
            'id' => 'required',
            'name' => 'required',
            'position' => 'required',
            'stat' => 'required'
        ]);
        if (!$validator->fails()) {

            $player_uID = only_numbers($player['id']);
            $player_exist = app(Player::class)->where('code', '=', $player_uID)
                ->count();

            if (!$player_exist) {
                $Player = app(Player::class)->create([
                    'team_id' => $team_id,
                    'code' => $player_uID,
                    'name' => $player['name'],
                    'position' => $player['position']
                ]);
                if (isset($player['stat'])) {
                    $this->savePlayerStats($Player->code, $player['stat']);
                }
            }

        }
    }

    /**
     * @param $player_id
     * @param array $stats
     */
    public function savePlayerStats($player_id, array $stats)
    {
        foreach ($stats as $stat) {
            $stat_exist = app(PlayerStat::class)->where([
                'key' => $stat['@Type'],
                'player_id' => $player_id
            ])->count();
            if (!$stat_exist) {
                app(PlayerStat::class)->create([
                    'player_id' => $player_id,
                    'key' => $stat['@Type'],
                    'value' => isset($stat['#text']) ? $stat['#text'] : 'empty'
                ]);
            }
        }
    }

    /**
     * @param $group_name
     * @param $team_data
     */
    public function saveGroups($group_name = '', $team_data)
    {
        if ($this->Competition instanceof Competition) {
            $exists = app(CompetitionTeam::class)->get(['team_id', 'competition_id']);
            foreach ($team_data as $team_datum) {

                $team_id = only_numbers($team_datum['@TeamRef']);

                $exist = $exists->where('competition_id', $this->Competition->code)->where('team_id', $team_id)->first();
                if (!$exist) {
                    app(CompetitionTeam::class)->create([
                        'competition_id' => $this->Competition->code,
                        'team_id' => only_numbers($team_datum['@TeamRef']),
                        'group_name' => $group_name
                    ]);
                } else {
                    $exist->update([
                        'group_name' => $group_name
                    ]);
                }
            }
        }
    }

    /**
     * @param array $matchData
     * @return $this
     * @throws PerformException
     */
    public function saveMatchData(array $matchData)
    {
        if ($this->Competition instanceof Competition) {
            foreach ($matchData as $matchDatum) {

                $MatchInfo = $matchDatum['MatchInfo'];

                $TeamData = $matchDatum['TeamData'];

                $this->saveGroups(isset($MatchInfo['@GroupName']) ? $MatchInfo['@GroupName'] : '', $TeamData);

                $CollectTeamData = collect($TeamData);

                $HomeTeam = $CollectTeamData->where('@Side', 'Home')->first();
                $HomeTeam = only_numbers($HomeTeam['@TeamRef']);

                $AwayTeam = $CollectTeamData->where('@Side', 'Away')->first();
                $AwayTeam = only_numbers($AwayTeam['@TeamRef']);

                $ScoreHome = isset($TeamData[0]['@Score']) ? $TeamData[0]['@Score'] : 0;
                $ScoreAway = isset($TeamData[1]['@Score']) ? $TeamData[1]['@Score'] : 0;

                $Period = $MatchInfo['@Period'];
                $FirstMatchID = isset($MatchInfo['@FirstLegId']) ? only_numbers($MatchInfo['@FirstLegId']) : null;
                $MatchType = isset($MatchInfo['@MatchType']) ? $MatchInfo['@MatchType'] : null;

                $TotalGoals = $ScoreHome + $ScoreAway;

                $MatchWinner = isset($MatchInfo['@MatchWinner']) ? only_numbers($MatchInfo['@MatchWinner']) : null;

                $match_uID = only_numbers($matchDatum['@uID']);
                $Match = Game::where('code', '=', $match_uID)->first();

                if (!$Match) {
                    Game::create([
                        'away_team_id' => $AwayTeam,
                        'home_team_id' => $HomeTeam,
                        'code' => $match_uID,
                        'first_match_id' => $FirstMatchID,
                        'competition_id' => $this->Competition->code,
                        'next_match_id' => isset($MatchInfo['@NextMatch']) ? only_numbers($MatchInfo['@NextMatch']) : null,
                        'match_winner' => $MatchWinner,
                        'score_home' => $ScoreHome,
                        'score_away' => $ScoreAway,
                        'date' => londonToBr(isset($MatchInfo['Date']['#text']) ? $MatchInfo['Date']['#text'] : $MatchInfo['Date']),
                        'total_goals' => $TotalGoals,
                        'ended' => !is_null($MatchWinner) ? 1 : 0,
                        'period' => $Period,
                        'match_type' => $MatchType
                    ]);
                } else {
                    $Match->update([
                        'first_match_id' => $FirstMatchID,
                        'match_winner' => $MatchWinner,
                        'score_home' => $ScoreHome,
                        'score_away' => $ScoreAway,
                        'total_goals' => $TotalGoals,
                        'ended' => !is_null($MatchWinner) ? 1 : 0,
                        'date' => londonToBr(isset($MatchInfo['Date']['#text']) ? $MatchInfo['Date']['#text'] : $MatchInfo['Date']),
                        'period' => $Period,
                        'match_type' => $MatchType
                    ]);
                }
                $this->MatchData[] = $match_uID;

                if (isset($matchDatum['MatchOfficials'])) {
                    $this->saveMatchOfficials($match_uID, $matchDatum['MatchOfficials']);
                }
            }
        } else {
            throw new PerformException('A Competição ainda não foi definida.');
        }
        return $this;
    }

    /**
     * @param $match_id
     * @param array $officials
     */
    public function saveMatchOfficials($match_id, array $officials)
    {
        $exists = app(Official::class)->get(['code']);
        foreach ($officials as $official) {

            $officialObject = isset($official['MatchOfficial']) ? $official['MatchOfficial'] : $official;

            if (!isset($officialObject['@uID'])) {
                foreach ($official as $item) {
                    $this->saveMatchOfficials($match_id, ['MatchOfficial' => $item]);
                }
            } else {
                $official_uID = only_numbers($officialObject['@uID']);
                $exist = $exists->where('code', $official_uID)->first();

                if (!$exist) {
                    $this->Officials[] = app(Official::class)->create([
                        'code' => $official_uID,
                        'name' => $officialObject['@FirstName'],
                        'last_name' => $officialObject['@LastName'],
                        'type' => $officialObject['@Type']
                    ]);

                } else {
                    $this->Officials[] = $exist;
                }

                MatchOfficial::firstOrCreate(['match_id' => $match_id, 'official_id' => $official_uID]);
            }
        }
    }
}