<?php
/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 28/03/2018
 * Class F1
 * @package Lik3\Perform
 *
 * Competição e temporada, inclui qualquer referência de partida ( passado ou presente )
 */

namespace Lik3\Perform\Feeds;

use Lik3\Perform\Contracts\PerformInterface;
use Lik3\Perform\Exceptions\PerformException;

class F1 extends Base implements PerformInterface
{
    public function fileConvention(string $file_name)
    {
        return $this;
    }

    /**
     * @param $payload
     * @return mixed|void
     * @throws PerformException
     */
    public function parse($payload)
    {
        $this->setPayload($payload)
            ->setSoccerDocument($this->payload->get('SoccerDocument'));

        $this->round = $this->soccerDocument->get('@CurrentRound');

        $this->saveSeason([
            'id' => $this->soccerDocument->get('@season_id'),
            'name' => $this->soccerDocument->get('@season_name')
        ])->saveCompetition([
            'code' => only_numbers($this->soccerDocument->get('@competition_id')),
            'name' => $this->soccerDocument->get('@competition_name'),
            'slug_code' => $this->soccerDocument->get('@competition_code')
        ])->saveTeams($this->soccerDocument->get('Team'));

        if ($this->soccerDocument->get('MatchData'))
            $this->saveMatchData($this->soccerDocument->get('MatchData'));
    }

}