<?php
/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 29/03/2018
 */

namespace Lik3\Perform\Feeds;


use Lik3\Perform\Contracts\PerformInterface;
use Lik3\Perform\Exceptions\PerformException;

class F40 extends Base implements PerformInterface
{

    private $competition_id;

    private $season_id;

    /**
     * @param string $file_name
     * @return $this|mixed
     * @throws PerformException
     *
     * srml-{competition_id}-{season_id}-squads.xml
     */
    public function fileConvention(string $file_name)
    {
        preg_match('/srml-(?<competition_id>[0-9]+)-(?<season_id>[0-9]+)-squads.xml/', $file_name, $return);
        $validator = validator($return, ["competition_id" => "required", "season_id" => "required"]);
        if (!$validator->fails()) {
            $this->competition_id = (int)$return["competition_id"];
            $this->season_id = (int)$return["season_id"];
        } else {
            throw new PerformException(json_encode($validator->failed()));
        }
        return $this;
    }

    /**
     * @param $payload
     * @return mixed|void
     * @throws PerformException
     */
    public function parse($payload)
    {
        $this->setPayload($payload)
            ->setSoccerDocument($this->payload->get('SoccerDocument'));

        $this->saveSeason([
            'id' => $this->soccerDocument->get('@season_id'),
            'name' => $this->soccerDocument->get('@season_name')
        ])->saveCompetition([
            'code' => only_numbers($this->soccerDocument->get('@competition_id')),
            'name' => $this->soccerDocument->get('@competition_name'),
            'slug_code' => $this->soccerDocument->get('@competition_code')
        ])->saveTeams($this->soccerDocument->get('Team'));
    }

}