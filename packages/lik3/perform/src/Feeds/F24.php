<?php
/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 29/03/2018
 */

namespace Lik3\Perform\Feeds;


use App\Competition;
use App\EventQualifier;
use App\Game;
use App\GameEvent;
use App\Season;
use Lik3\Perform\Contracts\PerformInterface;
use Lik3\Perform\Exceptions\PerformException;

class F24 extends Base implements PerformInterface
{

    public $game;

    public $Game;

    public function fileConvention(string $file_name)
    {
        return $this;
    }

    /**
     * @param array $game
     * @return $this
     *
     * Apenas adiciona a variável da partida em uma instânciada da Classe
     */
    public function setGame(array $game)
    {
        $this->game = collect($game);
        return $this;
    }

    /**
     * @param array $game
     * @return $this
     * @throws PerformException
     */
    public function saveGame(array $game)
    {
        if ($this->Competition instanceof Competition && $this->Season instanceof Season) {
            $this->Game = app(Game::class)->createOrReturn([
                "code" => only_numbers($game['@id']),
                "competition_id" => $this->Competition->code,
                "season_id" => $this->Season->code,
                "away_team_id" => (int)$game['@away_team_id'],
                "home_team_id" => (int)$game['@home_team_id'],
                "match_day" => (int)$game['@matchday']
            ], "code");
        } else {
            throw new PerformException("Competição ou temporada não foi instanciado.");
        }
        return $this;
    }

    /**
     * @param array $events
     * @return $this
     * @throws PerformException
     */
    public function saveEvents(array $events)
    {
        if ($this->Game instanceof Game) {

            # Procuro todos os eventos, apenas o código
            $gameEvents = app(GameEvent::class)->get(["code"]);

            foreach ($events as $event) {
                # Converto o ID do evento para um número
                $event_id = only_numbers($event['@id']);
                # Utilizando a collection, verifico se o código que está sendo passado existe.
                $exist = $gameEvents->where('code', $event_id)->count();
                # Não existe, cria.
                if (!$exist) {

                    app(GameEvent::class)->create([
                        "code" => $event_id,
                        "game_id" => $this->Game->code,
                        "player_id" => isset($event['@player_id']) ? only_numbers($event['@player_id']) : null,
                        "team_id" => only_numbers($event['@team_id']),
                        "period_id" => only_numbers($event['@period_id']),
                        "event_id" => only_numbers($event['@event_id']),
                        "type_id" => only_numbers($event['@type_id']),
                        "outcome" => only_numbers($event['@outcome']),
                        "x" => (float)$event['@x'],
                        "y" => (float)$event['@y'],
                    ]);


//                    if (isset($event['Q'])) {
//                        $this->saveQualifiers($event['@id'], $event['Q']);
//                    }

                }
            }
        } else {
            $this->saveGame($this->game->toArray())
                ->saveEvents($events);
        }
        return $this;
    }

    /**
     * @param int $event_id
     * @param array $qualifiers
     * @return $this
     */
    public function saveQualifiers(int $event_id, array $qualifiers)
    {
        foreach ($qualifiers as $qualifier) {
            if (isset($qualifier['@id'])) {
                $exist = app(EventQualifier::class)->where('code', '=', $qualifier['@id'])->count();
                if ($exist < 1) {
                    app(EventQualifier::class)->create([
                        "code" => (int)$qualifier['@id'],
                        "qualifier_id" => (int)$qualifier['@qualifier_id'],
                        "event_id" => (int)$event_id,
                        "value" => isset($qualifier['@value']) ? $qualifier['@value'] : null
                    ]);
                }
            }
        }
        return $this;
    }

    /**
     * @param $payload
     * @return mixed|void
     * @throws \Lik3\Perform\Exceptions\PerformException
     */
    public function parse($payload)
    {
        $this->setPayload($payload)
            ->setGame($this->payload->get('Game'))
            ->saveSeason([
                "id" => $this->game->get('@season_id'),
                "name" => $this->game->get('@season_name')
            ])
            ->saveCompetition([
                "code" => only_numbers($this->game->get("@competition_id")),
                "name" => $this->game->get("@competition_name")
            ])
            ->saveEvents($this->game->get('Event'));
    }

}