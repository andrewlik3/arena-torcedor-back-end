<?php
/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 03/04/2018
 */

if (!function_exists('only_numbers')) {
    /**
     * @param $value
     * @return null|string|string[]
     *
     * Retorna apenas números de uma string
     */
    function only_numbers($value)
    {
        return preg_replace('/[^0-9]/', '', $value);
    }
}

if (!function_exists('londonToBr')) {
    /**
     * @param $londonDate
     * @return string
     *
     * Qualquer data passada será compreendida como se fosse da timezone London e em seguida convertida para Sao Paulo
     */
    function londonToBr($londonDate)
    {

        $londonDate = \Carbon\Carbon::parse($londonDate, 'Europe/London');
        $londonDate->tz = 'America/Sao_Paulo';

        return $londonDate->format('Y-m-d H:i:s');
    }
}