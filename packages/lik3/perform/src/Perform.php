<?php

namespace Lik3\Perform;

use Illuminate\Console\Command;
use Lik3\Perform\Feeds\F1;
use Lik3\Perform\Contracts\PerformInterface;
use Lik3\Perform\Exceptions\PerformException;
use Lik3\Perform\Feeds\F13;
use Lik3\Perform\Feeds\F24;
use Lik3\Perform\Feeds\F3;
use Lik3\Perform\Feeds\F40;
use Lik3\Perform\Feeds\F9;
use Lik3\Perform\Traits\File;
use Nathanmac\Utilities\Parser\Parser;

/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 28/03/2018
 * Class Perform
 * @package Lik3\Perform
 */
class Perform
{

    use File;

    public $Command;

    /**
     * Para facilitar pode instanciar o Peform fornecendo o arquivo e pasta
     * ou
     * Utilizar setFile(), setStorage()
     *
     * Perform constructor.
     * @param string $file
     * @param string $storage
     */
    public function __construct(string $file = "", string $storage = "")
    {
        set_time_limit(0);

        $this->setStorage($storage)
            ->setFile($file);
    }

    /**
     * @param Command $command
     * @return $this
     */
    public function setCommand($command)
    {
        $this->Command = $command;
        return $this;
    }

    /**
     * @param $payload
     * @param PerformInterface $perform
     * @return mixed
     */
    public function parse($payload, PerformInterface $perform)
    {
        return $perform->setCommand($this->Command)->fileConvention($this->getFile())->parse($payload);
    }

    /**
     * @param PerformInterface $perform
     * @return mixed
     * @throws PerformException
     */
    public function payload(PerformInterface $perform)
    {
        if (!$this->checkFileExists())
            throw new PerformException("Pasta ou arquivo estão vazios.");

        return $this->parse(app(Parser::class)->xml($this->getContent()), $perform);
    }

    /**
     * @throws PerformException
     *
     * Roda o arquivo único
     */
    public function run()
    {
        $return = $this->readTypeFile(file_get_contents($this->getPathFile()));
        if (isset($return["perform"])) {

            $this->Command->text("Rodando o feed {$return['perform']}.");

            $this->{$return["perform"]}();

            $this->Command->text("Leitura finalizada.");
        } else {
            throw new PerformException("Não encontra uma função específica");
        }
    }

    /**
     * @param $content
     * @return mixed
     */
    public function readTypeFile($content)
    {
        preg_match('/Opta::Feed::XML::(?<type>[a-zA-Z]+)::(?<perform>[a-zA-Z0-9]+)/', $content, $return);
        return $return;
    }

    public function specific($specific = "")
    {
        $files = \File::allFiles(storage_path("opta"));
        foreach ($files as $file) {

            $return = $this->readTypeFile(file_get_contents($file->getRealPath()));

            $this->setFile($file->getFileName());

            if (isset($return["perform"])) {

                if (strtoupper($specific) == $return["perform"]) {

                    $this->Command->text($file->getFileName() . " lendo...");
                    $this->Command->text("Função {$return['perform']}");

                    $this->{$return["perform"]}();

                    $this->addRead($file->getFileName());

                    $this->Command->text("Leitura finalizada {$file->getFileName()}.");
                }

            }
        }
    }

    /**
     * leitura automática dos arquivos
     */
//    public function automatic()
//    {
//        $files = \File::allFiles(storage_path("opta"));
//        foreach ($files as $file) {
//            if (!$this->checkIsRead($file->getFileName())) {
//
//                $return = $this->readTypeFile(file_get_contents($file->getRealPath()));
//
//                $this->setFile($file->getFileName());
//
//                $this->Command->text($file->getFileName() . " lendo...");
//
//                if (isset($return["perform"])) {
//
//                    if ($return["perform"] != "F24") {
//                        $this->Command->text("Função {$return['perform']}");
//                        $this->{$return["perform"]}();
//                        $this->addRead($file->getFileName());
//                    }
//
//                    $this->Command->text("Leitura finalizada {$file->getFileName()}.");
//                }
//            } else {
//                $this->Command->text("Arquivo {$file->getFileName()} já lido.");
//            }
//        }
//    }

    public function automatic()
    {
        $files = \File::allFiles(storage_path("opta"));
        foreach ($files as $file) {

            $return = $this->readTypeFile(file_get_contents($file->getRealPath()));

            $this->setFile($file->getFileName());

            $this->Command->text($file->getFileName() . " lendo...");

            if (isset($return["perform"])) {

                $this->Command->text("Função {$return['perform']}");
                $this->{$return["perform"]}();
                $this->addRead($file->getFileName());

                $this->Command->text("Leitura finalizada {$file->getFileName()}.");
            }
        }
    }

    /**
     * @return mixed
     * @throws PerformException
     */
    public function f1()
    {
        $this->payload(new F1());
        return $this;
    }

    /**
     * @return mixed
     * @throws PerformException
     */
    public function f3()
    {
        $this->payload(new F3());
        return $this;
    }

    /**
     * @return mixed
     * @throws PerformException
     */
    public function f9()
    {
        $this->payload(new F9());
        return $this;
    }


    /**
     * @return mixed
     * @throws PerformException
     */
    public function F13()
    {
        $this->payload(new F13());
        return $this;
    }


    /**
     * @return mixed
     * @throws PerformException
     */
    public function F24()
    {
        $this->payload(new F24());
        return $this;
    }

    /**
     * @return mixed
     * @throws PerformException
     */
    public function F40()
    {
        $this->payload(new F40());
        return $this;
    }

}