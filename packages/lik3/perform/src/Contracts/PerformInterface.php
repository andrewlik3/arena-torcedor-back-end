<?php

namespace Lik3\Perform\Contracts;

/**
 * Interface PerformInterface
 *
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 */
interface PerformInterface
{
    /**
     * @param $command
     * @return mixed
     */
    public function setCommand($command);

    /**
     * @param string $file_name
     * @return mixed
     */
    public function fileConvention(string $file_name);

    /**
     * @param $payload
     * @return mixed
     */
    public function setPayload($payload);

    /**
     * @param $payload
     * @return mixed
     */
    public function parse($payload);

}