<?php

namespace Lik3\Perform\Traits;

use Illuminate\Support\Facades\Storage;

/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 28/03/2018
 */
trait File
{

    /**
     * Pasta de leitura dos arquivos
     *
     * @var
     */
    private $storage = "";

    /**
     * Nome do arquivo que será lido
     *
     * @var string
     */
    private $file = "";

    /**
     * A pasta que vai ser utilizada como leitura, exemplo: {storage}/arquivo1
     *
     * @param string $value
     * @return $this
     */
    public function setStorage(string $value): self
    {
        $this->storage = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * Seta o nome do arquivo
     *
     * @param string $value
     * @return $this
     */
    public function setFile(string $value)
    {
        $this->file = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Monta o caminho completo do arquivo com a pasta
     *
     * @return string
     */
    public function getPathFile()
    {
        return $this->getStorage() . "/" . $this->getFile();
    }

    /**
     * Verifica se o arquivo existe
     *
     * @return bool
     */
    public function checkFileExists()
    {
        return $this->getFile() == "" || $this->getStorage() == "" ? false : (is_file($this->getPathFile()) ? true : false);
    }

    public function getContent()
    {
        if ($this->checkFileExists())
            return file_get_contents($this->getPathFile());
        return false;
    }

    /**
     * @param $read
     * @return bool
     */
    public function checkIsRead($read)
    {
        if (Storage::get('sync-opta-files.txt')) {
            $exist =  collect(explode(PHP_EOL, Storage::get('sync-opta-files.txt')));
            if ($exist->search($read) === false) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $file_name
     */
    public function addRead($file_name)
    {
        Storage::disk('local')->append('sync-opta-files.txt', $file_name);
    }
}