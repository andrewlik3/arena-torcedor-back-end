<?php
Route::get('teams', 'TeamController@index');
Route::get('competitions', 'CompetitionController@index');

Route::group([
    'prefix' => 'utils'
], function($route) {
    $route->get('date', 'UtilsController@date');
    $route->get('base-uri', 'UtilsController@baseUri');
});

Route::group([
    'prefix' => '{competition_code}'
], function($route) {
    // Classificações do campeonato
    $route->get('rank', 'CompetitionController@rank');
    // Todos os times do campeonato
    $route->get('teams', 'CompetitionController@teams');
    // Grupos do campeonato
    $route->get('groups', 'CompetitionController@groups');
    // Partidas do campeonato
    $route->get('matches', 'CompetitionController@matches');
    // Paginação das próximas partidas por campeonato
    $route->get('paginate-next-match', 'CompetitionController@paginateNextMatch');
    // Paginação das partidas anteriores por campeonato
    $route->get('paginate-previous-match', 'CompetitionController@paginatePreviousMatch');
    // Próxima partida
    $route->get('next-matches', 'CompetitionController@nextMatches');
    // Partida anterior
    $route->get('previous-matches', 'CompetitionController@previousMatches');
    // Partidas do dia/mes/ano
    $route->get('{day}/{month}/{year}/matches', 'CompetitionController@matchByDate');
});

Route::group([
    'prefix' => '{match_code}'
], function($route) {
    // Partida pelo id da partida
    $route->get('match', 'MatchController@match');
    // Mapa de calor pelo id da partida primeiro tempo, segundo tempo ou total
    $route->get('heatmap/{period?}', 'MatchController@statistics');
    // Mapa de calor por id da partida e jogador primeiro tempo, segundo tempo ou total
    $route->get('player/{player_code}/heatmap/{period?}', 'MatchController@player_statistics');
    // Mapa de calor por time pelo id da partida, primeiro tempo, segundo tempo ou total
    $route->get('team/{team_code}/heatmap/{period?}', 'MatchController@team_statistics');

});

Route::group([
    'prefix' => '{player_code}'
], function($route) {
    // Sobre o jogador utilizar o ID
    $route->get('about', 'PlayerController@about');
});