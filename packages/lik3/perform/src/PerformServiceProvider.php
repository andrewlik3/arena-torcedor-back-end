<?php

namespace Lik3\Perform;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class PerformServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Lik3\Perform\Api';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->map();
        $this->registerHelpers();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(__DIR__ . '/routes/api.php');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Perform', function($app) {
            return new Perform();
        });
    }

    public function registerHelpers()
    {
        if (file_exists(base_path("packages/lik3/perform/src/Helpers/StringHelper.php"))) {
            require "Helpers/StringHelper.php";
        }
    }
}