<?php
/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 05/04/2018
 */

namespace Lik3\Perform\Api;

use App\GamePlayerStat;
use App\Player;
use App\PlayerStat;
use Exception;

class PlayerController
{

    /**
     * @param $player_id
     * @param Player $player
     * @return \Illuminate\Http\JsonResponse
     */
    public function about($player_id, Player $player)
    {
        try {

            $query = $player->whereCode($player_id)
                ->first(['code', 'name', 'position']);

            foreach (PlayerStat::wherePlayerId($player_id)->get(['key', 'value']) as $about) {
                $query->{$about->key} = $about->value;
            }

            return response()->json($player->response($query));
        } catch (Exception $e) {
            return response()->json(["error" => true, "message" => $e->getMessage()], 400);
        }
    }

}