<?php
/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 06/04/2018
 */

namespace Lik3\Perform\Api;

use App\Competition;
use App\Game;
use App\Match;
use Carbon\Carbon;
use Exception;

class CompetitionController
{

    /**
     * @param Competition $competition
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Competition $competition)
    {
        try {
            $query = $competition->orderBy('code', 'asc')->get();
            return response()->json($competition->response($query));
        } catch (Exception $e) {
            return response()->json(["error" => true, "message" => "Falha na comunição com o servidor."], 400);
        }
    }

    /**
     * @param $competition_code
     * @param Competition $competition
     * @return \Illuminate\Http\JsonResponse
     */
    public function groups($competition_code, Competition $competition)
    {
        try {

            $query = $competition->whereCode($competition_code)
                ->with(['CompetitionTeams' => function($teams) use ($competition_code) {
                    $teams->with(['TeamRecord' => function($team_record) use ($competition_code) {
                        $team_record->where('competition_id', '=', $competition_code)
                            ->select(['team_id', 'points', 'game_played', 'win', 'drawn', 'loose', 'gp', 'gc', 'sg', 'position', 'current_round']);
                    }, 'Team' => function($team) {
                        $team->select(['code', 'name']);
                    }])->orderBy('group_name')
                        ->where('group_name','!=', '')
                        ->select(['competition_id', 'team_id', 'group_name']);
                }])->get(["code", "name"]);

            return response()->json($competition->response($query));
        } catch (Exception $e) {
            return response()->json(["error" => true, "message" => "Falha na comunição com o servidor."], 400);
        }
    }

    /**
     * @param $competition_code
     * @param Competition $competition
     * @return \Illuminate\Http\JsonResponse
     */
    public function teams($competition_code, Competition $competition)
    {
        try {

            $query = $competition->with(['Teams' => function($q) {
                $q->orderBy("name")->select(["code", "name", "slug"]);
            }])->whereCode($competition_code)
                ->get(["code", "name"]);

            return response()->json($competition->response($query));
        } catch (Exception $e) {
            return response()->json(["error" => true, "message" => "Falha na comunição com o servidor."], 400);
        }
    }

    /**
     * @param $competition_code
     * @param Competition $competition
     * @return \Illuminate\Http\JsonResponse
     */
    public function rank($competition_code, Competition $competition)
    {
        try {
            $query = $competition->with(['Rank' => function($q){
                $q->orderBy('position', 'asc')->select([
                    "competition_id", "team_id", "points", "position", "standing", "start_day_position", "drawn", "loose", "gc", "gp", "sg", "win"
                ])->with(["Team" => function($q) {
                    $q->select(["code", "name", "slug"]);
                }]);
            }])->whereCode($competition_code)
                ->get(["code", "name"]);

            return response()->json($competition->response($query));
        } catch (Exception $e) {
            return response()->json(["error" => true, "message" => "Falha na comunição com o servidor."], 400);
        }
    }

    /**
     * @param $competition_code
     * @param Competition $competition
     * @return \Illuminate\Http\JsonResponse
     */
    public function matches($competition_code, Competition $competition)
    {
        try {

            $query = $competition->with(['Matches' => function($q) {
                $q->with(['Away' => function($away) {
                    $away->select(["code", "name"]);
                }, 'Home' => function($home) {
                    $home->select(["code", "name"]);
                }, 'FirstMatch' => function($firstMatch) {
                    $firstMatch->select(["first_match_id", "total_goals", "match_winner", "date", "code", "score_home", "score_away"]);
                }])->orderBy('date', 'asc')
                    ->select(["code", "next_match_id", "competition_id", "date", "first_match_id", "home_team_id", "away_team_id", "ended", "date", "total_goals"]);
            }])->whereCode($competition_code)
                ->get(["code", "name"]);

            return response()->json($competition->response($query));
        } catch (Exception $e) {
            return response()->json(["error" => true, "message" => "Falha na comunição com o servidor."], 400);
        }
    }

    /**
     * @param $competition_code
     * @param Game $match
     * @return \Illuminate\Http\JsonResponse
     *
     * Paginação das partidas do dia atual em diante
     */
    public function paginateNextMatch($competition_code, Game $match)
    {
        try {


            $query = $match->with(['Away' => function($away) {
                $away->select(["code", "name"]);
            }, 'Home' => function($home) {
                $home->select(["code", "name"]);
            }, 'FirstMatch' => function($firstMatch) {
                $firstMatch->select(["first_match_id", "total_goals", "match_winner", "date", "code", "score_home", "score_away"]);
            }])->where('competition_id', '=', $competition_code)
                ->orderBy('date', 'asc')
                ->where('date', '>=', Carbon::now())
                ->select(["code", "next_match_id", "competition_id", "date", "first_match_id", "home_team_id", "away_team_id", "ended", "date", "total_goals", "score_home", "score_away"])
                ->paginate(1);

            return response()->json($match->response($query));
        } catch (Exception $e) {
            return response()->json(["error" => true, "message" => $e->getMessage()], 400);
        }
    }

    /**
     * @param $competition_code
     * @param Game $match
     * @return \Illuminate\Http\JsonResponse
     *
     * Paginação das partidas antigas
     */
    public function paginatePreviousMatch($competition_code, Game $match)
    {
        try {


            $query = $match->with(['Away' => function($away) {
                $away->select(["code", "name"]);
            }, 'Home' => function($home) {
                $home->select(["code", "name"]);
            }, 'FirstMatch' => function($firstMatch) {
                $firstMatch->select(["first_match_id", "total_goals", "match_winner", "date", "code", "score_home", "score_away"]);
            }])->where('competition_id', '=', $competition_code)
                ->orderBy('date', 'desc')
                ->where('date', '<=', Carbon::now())
                ->select(["code", "next_match_id", "competition_id", "date", "first_match_id", "home_team_id", "away_team_id", "ended", "date", "total_goals", "score_home", "score_away"])
                ->paginate(1);

            return response()->json($match->response($query));
        } catch (Exception $e) {
            return response()->json(["error" => true, "message" => $e->getMessage()], 400);
        }
    }

    /**
     * @param $competition_code
     * @param Competition $competition
     * @return \Illuminate\Http\JsonResponse
     *
     * Todas após o dial atual
     */
    public function nextMatches($competition_code, Competition $competition)
    {
        try {

            $query = $competition->with(['Matches' => function($q) {
                $q->with(['Away' => function($away) {
                    $away->select(["code", "name"]);
                }, 'Home' => function($home) {
                    $home->select(["code", "name"]);
                }, 'FirstMatch' => function($firstMatch) {
                    $firstMatch->select(["first_match_id", "total_goals", "match_winner", "date", "code", "score_home", "score_away"]);
                }])->where('date', '>=', Carbon::now())
                    ->orderBy('date', 'asc')
                    ->select(["code", "next_match_id", "competition_id", "date", "first_match_id", "home_team_id", "away_team_id", "ended", "date", "total_goals", "score_home", "score_away"]);
            }])->whereCode($competition_code)
                ->get(["code", "name"]);

            return response()->json($competition->response($query));
        } catch (Exception $e) {
            return response()->json(["error" => true, "message" => $e->getMessage()], 400);
        }
    }

    /**
     * @param $competition_code
     * @param Competition $competition
     * @return \Illuminate\Http\JsonResponse
     *
     * Todas as partidas antes do dia atual
     */
    public function previousMatches($competition_code, Competition $competition)
    {
        try {

            $query = $competition->with(['Matches' => function($q) {
                $q->with(['Away' => function($away) {
                    $away->select(["code", "name"]);
                }, 'Home' => function($home) {
                    $home->select(["code", "name"]);
                }, 'FirstMatch' => function($firstMatch) {
                    $firstMatch->select(["first_match_id", "total_goals", "match_winner", "date", "code", "score_home", "score_away"]);
                }])->where('date', '<=', Carbon::now())
                    ->orderBy('date', 'asc')
                    ->select(["code", "next_match_id", "competition_id", "date", "first_match_id", "home_team_id", "away_team_id", "ended", "date", "total_goals", "score_home", "score_away"]);
            }])->whereCode($competition_code)
                ->get(["code", "name"]);

            return response()->json($competition->response($query));
        } catch (Exception $e) {
            return response()->json(["error" => true, "message" => $e->getMessage()], 400);
        }
    }

    /**
     * @param $competition_code
     * @param $day
     * @param $month
     * @param $year
     * @param Match $match
     * @return \Illuminate\Http\JsonResponse
     *
     * Partidas correspondente a data passada por uri
     */
    public function matchByDate($competition_code, $day, $month, $year, Game $match)
    {
        try {

            $query = $match->with(['Away' => function($away) {
                $away->select(["code", "name"]);
            }, 'Home' => function($home) {
                $home->select(["code", "name"]);
            }, 'FirstMatch' => function($firstMatch) {
                $firstMatch->select(["first_match_id", "total_goals", "match_winner", "date", "code", "score_home", "score_away"]);
            }])->where('competition_id', '=', $competition_code)
                ->orderBy('date', 'asc')
                ->where(function($q) use ($day, $month, $year){
                    $q->whereYear('date', '=', $year)
                        ->whereMonth('date', '=', $month)
                        ->whereDay('date', '=', $day);
                })
                ->select(["code", "next_match_id", "competition_id", "date", "first_match_id", "home_team_id", "away_team_id", "ended", "date", "total_goals", "score_home", "score_away"])
                ->paginate(1);

            return response()->json($match->response($query));
        } catch (Exception $e) {
            return response()->json(["error" => true, "message" => $e->getMessage()], 400);
        }
    }

}