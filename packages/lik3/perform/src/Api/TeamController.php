<?php
/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 05/04/2018
 */

namespace Lik3\Perform\Api;

use App\Team;

class TeamController
{

    /**
     * @param Team $team
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Team $team)
    {
        try {
            $query = $team->get(["code", "name", "country", "founded"]);
            return response()->json($team->response($query));
        } catch (\Exception $e) {
            return response()->json(["error" => true, "message" => "Falha na comunição com o servidor."], 400);
        }
    }

}