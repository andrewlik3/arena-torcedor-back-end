<?php
/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 10/04/2018
 */

namespace Lik3\Perform\Api;


use Carbon\Carbon;

class UtilsController
{

    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * Prefixos dos endpoints disponíveis
     */
    public function baseUri()
    {
        $base_url = url("");
        return response()->json([
            "error" => false,
            "data" => [
                "uri" => str_finish($base_url, "/"),
                "api" => str_finish($base_url, "/api/"),
                "utils" => str_finish($base_url, "/utils/")
            ]
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * Pega a data por servidor
     */
    public function date()
    {
        $now = Carbon::now();
        return response()->json([
            "error" => false,
            "data" => [
                "day" => $now->day,
                "month" => $now->month,
                "year" => $now->year,
                "hour" => $now->hour,
                "minute" => $now->minute,
                "seconds" => $now->second,
                "today" => $now->format('d/m/Y'),
                "now" => $now->format('d/m/Y H:i:s')
            ]
        ]);
    }

}