<?php
/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 10/04/2018
 */

namespace Lik3\Perform\Api;

use App\Game;
use App\GameEvent;
use Exception;

class MatchController
{

    /**
     * @param $match_id
     * @param Game $match
     * @return \Illuminate\Http\JsonResponse
     */
    public function match($match_id, Game $match)
    {
        try {

            $query = $match->where("code", "=", $match_id)
                ->with(['Players', 'Competition' => function ($competition) {
                    $competition->select(['code', 'name']);
                }, 'Away' => function ($away) {
                    $away->select(['name', 'code']);
                }, 'Home' => function ($home) {
                    $home->select(['name', 'code']);
                }, 'Winner' => function ($winner) {
                    $winner->select(['name', 'code']);
                }, 'Messages' => function ($messages) {
                    $messages->orderBy('id')->select(['game_id', 'message', 'minute', 'period', 'second', 'time', 'type']);
                }])->first();

            return response()->json($match->response($query));
        } catch (Exception $e) {
            return response()->json(["error" => true, "message" => $e->getMessage()], 400);
        }
    }

    /**
     * @param $match_id
     * @param null $period
     * @param GameEvent $gameEvent
     * @return \Illuminate\Http\JsonResponse
     */
    public function statistics($match_id, $period = null, GameEvent $gameEvent)
    {
        try {

            $gameEvent->width = request()->filled('width') ? request('width') : $gameEvent->width;
            $gameEvent->height = request()->filled('height') ? request('height') : $gameEvent->height;

            $query = $gameEvent->selectRaw('`x`, `y`, sum(`value`) as `total`')
                ->where('x', '>', 0)
                ->where('y', '>', 0)
                ->whereGameId($match_id)
                ->where(function($q) use ($period){
                    if (!is_null($period))
                        $q->where('period_id', '=', $period);
                })
                ->orderBy('total', 'desc')
                ->groupBy(['x', 'y'])
                ->get();

            return response()->json($gameEvent->response($query, ['radius_heatmap' => 5]));
        } catch (Exception $e) {
            return response()->json(["error" => true, "message" => $e->getMessage()], 400);
        }
    }

    /**
     * @param $match_id
     * @param $player_id
     * @param null $period
     * @param GameEvent $gameEvent
     * @return \Illuminate\Http\JsonResponse
     */
    public function player_statistics($match_id, $player_id, $period = null, GameEvent $gameEvent)
    {
        try {

            $gameEvent->width = request()->filled('width') ? request('width') : $gameEvent->width;
            $gameEvent->height = request()->filled('height') ? request('height') : $gameEvent->height;

            $query = $gameEvent->selectRaw('`x`, `y`, sum(`value`) as `total`')
                ->where('x', '>', 0)
                ->where('y', '>', 0)
                ->wherePlayerId($player_id)
                ->whereGameId($match_id)
                ->where(function($q) use ($period){
                    if (!is_null($period))
                        $q->where('period_id', '=', $period);
                })
                ->orderBy('total', 'desc')
                ->groupBy(['x', 'y'])
                ->get();

            return response()->json($gameEvent->response($query, ['radius_heatmap' => 1.5]));
        } catch (Exception $e) {
            return response()->json(["error" => true, "message" => $e->getMessage()], 400);
        }
    }

    /**
     * @param $match_id
     * @param $team_id
     * @param null $period
     * @param GameEvent $gameEvent
     * @return \Illuminate\Http\JsonResponse
     */
    public function team_statistics($match_id, $team_id, $period = null, GameEvent $gameEvent)
    {
        try {

            $gameEvent->width = request()->filled('width') ? request('width') : $gameEvent->width;
            $gameEvent->height = request()->filled('height') ? request('height') : $gameEvent->height;

            $query = $gameEvent->selectRaw('`x`, `y`, sum(`value`) as `total`')
                ->where('x', '>', 0)
                ->where('y', '>', 0)
                ->whereTeamId($team_id)
                ->whereGameId($match_id)
                ->where(function($q) use ($period){
                    if (!is_null($period))
                        $q->where('period_id', '=', $period);
                })
                ->orderBy('total', 'desc')
                ->groupBy(['x', 'y'])
                ->get();

            return response()->json($gameEvent->response($query, ['radius_heatmap' => 5]));
        } catch (Exception $e) {
            return response()->json(["error" => true, "message" => $e->getMessage()], 400);
        }
    }

}