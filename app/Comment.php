<?php

namespace App;



class Comment extends BaseModel
{

    protected $fillable = [
        "home_team_id", "away_team_id", "competition_id", "game_id",
        "session_id", "match_day", "season_id", "match_date"
    ];

    public function Messages()
    {
        return $this->hasMany(CommentMessage::class, 'game_id', 'game_id');
    }

}
