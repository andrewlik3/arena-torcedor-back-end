<?php

namespace App;



class CommentMessage extends BaseModel
{

    protected $fillable = [
        "game_id", "code", "message", "minute", "period",
        "second", "time", "type", "modified"
    ];

}
