<?php

namespace App;



class TeamRecord extends BaseModel
{

    protected $casts = [
        'standing' => 'array'
    ];

    protected $fillable = [
        'competition_id', 'team_id', 'standing', 'current_round',
        'points', 'position', 'start_day_position', 'loose', 'drawn', 'win',
        'gc', 'gp', 'sg'
    ];

    public function Team()
    {
        return $this->belongsTo(Team::class, "team_id", "code");
    }

}
