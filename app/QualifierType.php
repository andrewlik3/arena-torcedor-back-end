<?php

namespace App;



class QualifierType extends BaseModel
{

    protected $fillable = ["id", "event_name", "value", "description"];

}
