<?php

namespace App;

use App\Traits\ModelTrait;


class Season extends BaseModel
{

    use ModelTrait;

    protected $fillable = ["code", "name"];

}
