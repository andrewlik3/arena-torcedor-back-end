<?php

namespace App;

use App\Traits\JsonTrait;
use App\Traits\ModelTrait;
use Carbon\Carbon;


class Game extends BaseModel
{

    use ModelTrait, JsonTrait;

    protected $hidden = ['created_at', 'updated_at', 'away_team_id', 'home_team_id', 'match_winner', 'id', 'season_id'];

    protected $fillable = [
        "type", "season_id", "away_team_id", "home_team_id", "match_day", "code", "first_match_id",
        "competition_id", "competition_id", "next_match_id", "match_winner", "score_home", "score_away",
        "date", "total_goals", "ended", "period", "match_type"
    ];

    protected $appends = ['live'];

    protected $dates = ['date', 'updated_at', 'created_at'];

    public function getLiveAttribute()
    {
        $date = Carbon::parse($this->date);
        $diff = $date->diffInHours(Carbon::now(), false);
        if (($diff >= -1 && $diff < 2) && $this->ended == 0) {
            return true;
        } else  {
            return false;
        }
    }

    public function GameStatistics()
    {
        return $this->hasMany(GameEvent::class, 'game_id', 'code');
    }

    public function Winner()
    {
        return $this->belongsTo(Team::class, 'match_winner', 'code');
    }

    public function Players()
    {
        return $this->hasMany(GamePlayerLine::class, 'game_id', 'code');
    }

    public function Competition()
    {
        return $this->belongsTo(Competition::class, 'competition_id', 'code');
    }

    public function Away()
    {
        return $this->belongsTo(Team::class, 'home_team_id', 'code');
    }

    public function Home()
    {
        return $this->belongsTo(Team::class, 'away_team_id', 'code');
    }

    public function FirstMatch()
    {
        return $this->belongsTo(Match::class, 'first_match_id', 'code');
    }

    public function Comments()
    {
        return $this->hasMany(Comment::class, 'game_id', 'code');
    }

    public function Messages()
    {
        return $this->hasMany(CommentMessage::class, 'game_id', 'code');
    }

}