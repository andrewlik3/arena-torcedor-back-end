<?php

namespace App;

use App\Traits\ModelTrait;


class TeamOfficial extends BaseModel
{

    use ModelTrait;

    protected $fillable = [
        "code", "team_id", "name", "type",
        "last_name", "country", "birth_date"
    ];

}
