<?php

namespace App;



class ResultStat extends BaseModel
{

    protected $fillable = [
        "result_id", "type", "text"
    ];

}
