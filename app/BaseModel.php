<?php
/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 17/04/2018
 */

namespace App;

//use Jenssegers\Mongodb\Eloquent\Model;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{

}