<?php

namespace App;



class EventType extends BaseModel
{

    protected $fillable = ["event_type_id", "title", "description"];

}
