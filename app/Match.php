<?php

namespace App;

use App\Traits\JsonTrait;
use Carbon\Carbon;


class Match extends BaseModel
{

    use JsonTrait;

    protected $fillable = [
        'code', 'competition_id', 'match_winner', 'next_match_id', 'first_match_id', 'period',
        'home_team', 'away_team', 'date', 'total_goals', 'ended', 'match_type', 'score_home', 'score_away'
    ];

    protected $appends = ['live'];

    public function getLiveAttribute()
    {
        $match_date = Carbon::parse($this->date);
        return $match_date->diffInHours(Carbon::now());
    }

    public function Competition()
    {
        return $this->belongsTo(Competition::class, 'competition_id', 'code');
    }

    public function Away()
    {
        return $this->belongsTo(Team::class, 'home_team', 'code');
    }

    public function Home()
    {
        return $this->belongsTo(Team::class, 'away_team', 'code');
    }

    public function FirstMatch()
    {
        return $this->belongsTo(Match::class, 'first_match_id', 'code');
    }

    public function Comments()
    {
        return $this->hasMany(Comment::class, 'game_id', 'code');
    }

    public function Messages()
    {
        return $this->hasMany(CommentMessage::class, 'game_id', 'code');
    }
}
