<?php

namespace App;



class ResultMatchInfo extends BaseModel
{

    protected $fillable = [
        "result_id", "period", "match_type", "result_type",
        "winner_id"
    ];

}
