<?php

namespace App;

use App\Traits\JsonTrait;


class GameEvent extends BaseModel
{

    use JsonTrait;

    public $width = 1000;

    public $height = 500;

    protected $hidden = ["game_id"];

    protected $fillable = ["player_id", "code", "game_id", "team_id", "period_id", "event_id", "type_id", "outcome", "x", "y", "value"];

    private function formula($valor, $cordenada)
    {
        # valor * cordenada / 100
        return ($valor * $cordenada) / 100;
    }

    public function getXAttribute($value)
    {
        return $this->formula($this->width, $value);
    }

    public function getYAttribute($value)
    {
        return $this->formula($this->height, $value);
    }

}
