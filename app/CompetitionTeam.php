<?php

namespace App;

use App\Traits\ModelTrait;


class CompetitionTeam extends BaseModel
{

    use ModelTrait;

    protected $fillable = ["team_id", "competition_id", "group_name"];

    public function Team()
    {
        return $this->belongsTo(Team::class, 'team_id', 'code');
    }

    public function TeamRecord()
    {
        return $this->belongsTo(TeamRecord::class, 'team_id', 'team_id');
    }

}
