<?php

namespace App;

use App\Traits\ModelTrait;


class GamePlayerBook extends BaseModel
{

    use ModelTrait;

    protected $hidden = ['created_at', 'updated_at', 'id'];

    protected $fillable = [
        'code', 'player_id', 'card_type', 'reason', 'period',
        'min', 'sec', 'time'
    ];

}
