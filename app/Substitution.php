<?php

namespace App;

use App\Traits\ModelTrait;


class Substitution extends BaseModel
{

    use ModelTrait;


    protected $hidden = ['created_at', 'updated_at', 'game_id', 'id', 'player_on'];

    protected $fillable = [
        "game_id", "code", "player_on", "player_off",
        "reason", "period", "minute", "second"
    ];

    public function PlayerOn()
    {
        return $this->belongsTo(Player::class, 'player_on', 'code');
    }

    public function PlayerOff()
    {
        return $this->belongsTo(Player::class, 'player_off', 'code');
    }

}
