<?php

namespace App;



class ResultTeamDataGoal extends BaseModel
{

    protected $fillable = [
        "code", "type", "team_data_id", "event_id", "player_id",
        "event_number", "minute", "second", "player_assist_id"
    ];

}
