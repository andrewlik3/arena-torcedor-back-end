<?php

namespace App\Exceptions;

use Illuminate\Support\MessageBag;

class OptaPerformException
{
    public $messageBag;

    public function __construct()
    {
        $this->messageBag = new MessageBag();
    }

}