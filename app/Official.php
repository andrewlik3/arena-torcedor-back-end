<?php

namespace App;



class Official extends BaseModel
{

    protected $fillable = ["code", "name", "last_name", "type"];

}
