<?php

namespace App;

use App\Traits\JsonTrait;
use App\Traits\ModelTrait;


class Team extends BaseModel
{

    use ModelTrait, JsonTrait;

    protected $hidden = ["pivot"];

    protected $fillable = [
        "code", "name", "slug",
        "country", "founded"
    ];

}
