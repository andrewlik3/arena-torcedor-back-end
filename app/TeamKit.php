<?php

namespace App;

use App\Traits\ModelTrait;


class TeamKit extends BaseModel
{

    use ModelTrait;

    protected $fillable = ["team_id", "code", "color_one", "color_two", "color_tree", "type"];

}
