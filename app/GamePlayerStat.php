<?php

namespace App;

use App\Traits\JsonTrait;


class GamePlayerStat extends BaseModel
{

    use JsonTrait;

    protected $hidden = ['game_id', 'game_player_line_id'];

    protected $fillable = [
        "game_id", "game_player_line_id", "key", "value"
    ];

    protected $appends = ['key_translate'];

    private $translate = [
        'diving_save' => 'Defesas com mergulho',
        'leftside_pass' => 'Passe na lateral esquerda',
        'accurate_pass' => 'Passes com precisão',
        'total_final_third_passes' => '',
        'rightside_pass' => 'Passe na lateral direita',
        'attempts_conceded_inbox' => 'Tentativa de recebimento da bola na área',
        'touches' => 'Toques',
        'total_fwd_zone_pass' => 'Total de passes na zona do ataque',
        'keeper_pick_up' => 'Bolas agarradas',
        'accurate_fwd_zone_pass' => 'Precisão de passes na zona do ataque',
        'total_chipped_pass' => '',
        'lost_corners' => 'Escanteios perdidos',
        'goals_conceded' => 'Gols',
        'saves' => 'Defesas',
        'attempts_conceded_obox' => '',
        'ball_recovery' => 'Recuperação de bola',
        'turnover' => 'Rotatividade',
        'poss_won_def_3rd' => '',
        'accurate_back_zone_pass' => 'Precisão na zona defensiva',
        'dive_save' => '',
        'passes_right' => 'Passes certos',
        'successful_open_play_pass' => '',
        'stand_save' => '',
        'total_back_zone_pass' => 'Passes para zona defensiva',
        'total_long_balls' => 'Lançamentos de bolas longas',
        'accurate_keeper_throws' => '',
        'goals_conceded_ibox' => 'Gols na área',
        'goal_kicks' => 'Chute a Gol',
        'open_play_pass' => 'Passe de início de jogo',
        'total_pass' => 'Passes',
        'total_launches' => 'Lançamentos',
        'fwd_pass' => 'Passes para frente',
        'game_started' => 'Começou jogando',
        'long_pass_own_to_opp' => '',
        'accurate_chipped_pass' => '',
        'successful_final_third_passes' => '',
        'keeper_throws' => '',
        'passes_left' => '',
        'accurate_launches' => 'Precisão nos lançamentos',
        'poss_lost_all' => '',
        'accurate_long_balls' => 'Bolas longas com precisão',
        'accurate_goal_kicks' => 'Chute a Gols com precisão',
        'saved_obox' => '',
        'unsuccessful_touch' => 'Falha de toques',
        'poss_lost_ctrl' => '',
        'final_third_entries' => '',
        'effective_clearance' => 'Afastadas efetivas',
        'mins_played' => 'Tempo jogado',
        'goals_conceded_obox' => '',
        'total_clearance' => 'Bolas afastadas',
        'long_pass_own_to_opp_success' => '',
        'saved_ibox' => '',
        'punches' => 'Defesas com socos',
        'formation_place' => ''
    ];

    public function getKeyTranslateAttribute()
    {
        return isset($this->translate[$this->key]) && !empty($this->translate[$this->key]) ? $this->translate[$this->key] : null;
    }

}
