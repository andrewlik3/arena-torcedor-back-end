<?php

namespace App;

use App\Traits\JsonTrait;
use App\Traits\ModelTrait;


class Competition extends BaseModel
{

    use ModelTrait, JsonTrait;

    protected $fillable = ["code", "season_id", "code", "name", "slug_code"];


    public function Teams()
    {
        return $this->belongsToMany(Team::class, 'competition_teams', 'competition_id', 'team_id', 'code', 'code');
    }

    public function CompetitionTeams()
    {
        return $this->hasMany(CompetitionTeam::class, 'competition_id', 'code');
    }

    public function Rank()
    {
        return $this->hasMany(TeamRecord::class, 'competition_id', 'code');
    }

    public function Matches()
    {
        return $this->hasMany(Game::class, 'competition_id', 'code');
    }

}
