<?php

namespace App\Traits;

/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 28/03/2018
 */

trait ModelTrait
{
    /**
     * @param array $data
     * @param string $key
     * @return mixed
     * @throws \Exception
     *
     * Cria ou apenas retorna o que existe.
     * o método firstOrCreate verifica todos os campos passados
     */
    public function createOrReturn(array $data, $key = 'id')
    {
        # Verifico se existe
        $exist = $this->where([$key => $data[$key]])->first();
        if (!$exist) {
            # Caso não exista, limpa o cache
            cache()->forget($this->table);
            # Cria o conteúdo
            $this->create($data);
            # Procura pelo key e o valor
            $exist = $this->where($key,$data[$key])->first();
        } else {
            # Caso não exista, limpa o cache
            cache()->forget($this->table);
            # Atualiza
            $exist->update($data);
        }
        return $exist;
    }
}