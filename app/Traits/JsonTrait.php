<?php
/**
 * Author: Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>
 * Data: 05/04/2018
 */

namespace App\Traits;

use Illuminate\Support\Collection;

trait JsonTrait
{

    private $onlyAttributes = [];

    private $response;

    public function response($data, $extra = [])
    {
        return ["error" => false, "message" => "Registros encontrados.", "data" => $data] + $extra;
    }

}