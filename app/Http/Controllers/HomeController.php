<?php

namespace App\Http\Controllers;

use App\EventType;
use App\OptaPerform\F1;
use App\OptaPerform\F3;
use App\QualifierType;
use Lik3\Perform\Exceptions\PerformException;
use Lik3\Perform\Perform;
use Nathanmac\Utilities\Parser\Parser;
use Symfony\Component\DomCrawler\Crawler;

class HomeController extends Controller
{

    private $crawler;

//    public function index(F1 $f01, F3 $f03)
//    {
//
//        $f01->setFile(storage_path("opta/srml-4-2017-results.xml"))
//            ->run();
//
//        $f01->setFile(storage_path("opta/srml-5-2017-results.xml"))
//            ->run();
//
//        $f03->setFile(storage_path("opta/srml-365-2017-standings.xml"))
//            ->run();
//
//        $f03->setFile(storage_path("opta/srml-628-2017-standings.xml"))
//            ->run();
//
//
//
////        $this->F24();
//    }

    /**
     * @param Perform $perform
     * @throws PerformException
     */
    public function index(Perform $perform)
    {
//        $perform->setStorage(storage_path("opta"));
////            ->automatic();
//
//        $perform->setFile("srml-365-2017-f981268-matchresults.xml")->f9();


        $perform->setStorage(storage_path("opta"))
            ->setFile("srml-4-2017-results.xml")
            ->f1()
            ->setFile("srml-365-2017-standings.xml")
            ->f3()
            ->setFile("commentary-365-2017-981266-es.xml")
            ->f13()
            ->setFile("srml-420-2018-squads.xml")
            ->f40()
            ->setFile("srml-366-2017-f981637-matchresults.xml")
            ->f9()
            ->setFile("f24-365-2017-981265-eventdetails.xml")
            ->f24();
    }

    private function F01()
    {
        try {
            # Instância o Parser
            $parser = new Parser();
            # Lê o XML
//            $parsed = $parser->xml(file_get_contents(storage_path("srml-5-2013-results-group-competition-.xml")));
            $parsed = $parser->xml(file_get_contents(storage_path("carioca-1.xml")));
            # Lê o nó SoccerDocumento
            $parsed = $parsed['SoccerDocument'];

            dd($parsed);

            $competition_name = $parsed['@competition_name'];
            $competition_id = $parsed['@competition_id'];
            $season_id = $parsed['@season_id'];

            # Procura a competição pelo ID
            $competition = app($this->Competition)->find($competition_id);
            if (!$competition) {
                app($this->Competition)->create(["id" => $competition_id, "name" => $competition_name, "season_id" => $season_id]);
            }

            # Procura pelos times, para não repetir a inserção
            $existTeams = app($this->Team)->pluck("code", "id");
            foreach ($parsed["Team"] as $team) {
                if (!$existTeams->search($team['@uID'])) {
                    $t = app($this->Team)->create([
                        "code" => $team['@uID'],
                        "name" => $team['Name'],
                        "slug" => str_slug($team["Name"])
                    ]);
                    # Verifico se o time já não está vinculado a competição
                    $competitionTeamCheck = app($this->CompetitionTeam)->where(["team_id" => $t->id, "competition_id" => $competition_id])->count();
                    if (!$competitionTeamCheck) {
                        # Vinculo o time com a competição
                        app($this->CompetitionTeam)->create(["team_id" => $t->id, "competition_id" => $competition_id]);
                    }
                }
            }
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    private function F24()
    {
        $this->crawler = new Crawler();
        $this->crawler->addHtmlContent(file_get_contents(storage_path("praxis-24.html")));

        $collectTypes = $this->crawlerEQ();
        $exists = EventType::pluck("id")->toArray();
        $collectTypes->map(function ($m) use ($exists) {
            if (!in_array($m[0], $exists)) {
                $EventType = new EventType();
                $EventType->id = intval($m[0]);
                $EventType->title = $m[1];
                $EventType->description = $m[2];
                $EventType->save();
            }
        });

        $collectQualifier = $this->crawlerEQ(2);
        $exists = QualifierType::pluck("id")->toArray();
        $collectQualifier->map(function($m) use ($exists){
            if (is_numeric($m[0])) {
                if (!in_array($m[0], $exists)) {
                    $QualifierType = new QualifierType();
                    $QualifierType->id = $m[0];
                    $QualifierType->event_name = $m[1];
                    $QualifierType->value = $m[2];
                    $QualifierType->description = $m[3];
                    $QualifierType->save();
                }
            }
        });
    }

    private function crawlerEQ($eq = 0)
    {
        $eventTypes = $this->crawler->filter('table')->eq($eq)->filter('tr')->each(function ($tr, $i) {
            if ($i > 2) {
                return $tr->filter('td')->each(function ($td, $i) {
                    return trim($td->text());
                });
            }
        });

        return collect(array_filter($eventTypes));
    }

}
