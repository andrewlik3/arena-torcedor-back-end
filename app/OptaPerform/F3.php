<?php

namespace App\OptaPerform;

use App\Competition;
use App\Exceptions\OptaPerformException;
use App\TeamRecord;

class F3 extends OptaPerformException
{

    use ParseTrait;

    private $TeamStandings;

    private $TeamRecord;

    /**
     * @param array $value
     * @return $this
     */
    public function setTeamStandings(array $value)
    {
        $this->TeamStandings = $value;
        return $this;
    }

    /**
     * @param null $key
     * @return mixed
     */
    public function getTeamStandings($key = null)
    {
        return !is_null($key) ? (isset($this->TeamStandings[$key]) ? $this->TeamStandings[$key] : []) : $this->TeamStandings;
    }

    /**
     * @param array $value
     * @return $this
     */
    public function setTeamRecord(array $value)
    {
        $this->TeamRecord = $value;
        # Se existir uma competição setada
        if ($this->Competition instanceof Competition) {
            # Procuro todas as pontuações
            $Exists = TeamRecord::get(['id', 'competition_id', 'team_id']);
            foreach ($this->TeamRecord as $teamRecord) {
                # Aloco uma memoria para o Standing
                $standing = $teamRecord['Standing'];
                # Se não existir a relação do time e a competição, cria senão atualiza
                if ($Exists->where('competition_id', $this->Competition->id)->where('team_id', $teamRecord['@TeamRef'])->count() < 1) {
                    $soccerDocument = $this->getSoccerDocument();
                    # Cria
                    TeamRecord::create([
                        'current_round' => $soccerDocument['@CurrentRound'],
                        'competition_id' => $this->Competition->id,
                        'team_id' => $teamRecord['@TeamRef'],
                        'standing' => $standing
                    ]);
                } else {
                    $edit = $Exists->where('competition_id', $this->Competition->id)->where('team_id', $teamRecord['@TeamRef'])->first();
                    # Faço a procura por query
                    $edit = TeamRecord::find($edit->id);
                    # Atualizo
                    $edit->update([
                        'competition_id' => $this->Competition->id,
                        'team_id' => $teamRecord['@TeamRef'],
                        'standing' => $standing
                    ]);
                }
            }
        }
        # Retorna ele mesmo
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTeamRecord()
    {
        return $this->TeamRecord;
    }

    public function setup()
    {
        $this->setSoccerDocument($this->getParse('SoccerDocument'))
            ->setCompetition()
            ->setTeam($this->getSoccerDocument('Team'))
            ->setTeamStandings($this->getSoccerDocument('Competition')['TeamStandings'])
            ->setTeamRecord($this->getTeamStandings('TeamRecord'));
    }

    public function run()
    {
        $this->parse()
            ->setup();
    }

}