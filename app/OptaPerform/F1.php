<?php

namespace App\OptaPerform;

use App\Competition;
use App\Exceptions\OptaPerformException;
use App\Match;
use Illuminate\Support\Facades\Crypt;

class F1 extends OptaPerformException
{

    use ParseTrait;

    private $timingType;

    private $matchData;

    private $Match;


    /**
     * @return $this
     */
    public function setup()
    {
        $this->setSoccerDocument($this->getParse('SoccerDocument'))
            ->setCompetition()
            ->setTeam($this->getSoccerDocument('Team'))
            ->setMatchData($this->getSoccerDocument('MatchData'));

        return $this;
    }


    /**
     * @param array $value
     * @return $this
     */
    public function setMatchData(array $value)
    {
        $this->matchData = $value;

        try {

            foreach ($this->matchData as $matchDatum) {

                $MatchInfo = $matchDatum['MatchInfo'];

                $TeamData = $matchDatum['TeamData'];

                $HomeTeam = $TeamData[0]['@Side'] == 'Home' ? $TeamData[0]['@TeamRef'] : $TeamData[1]['@TeamRef'];
                $AwayTeam = $TeamData[0]['@Side'] == 'Away' ? $TeamData[0]['@TeamRef'] : $TeamData[1]['@TeamRef'];

                $ScoreHome = isset($TeamData[0]['@Score']) ? $TeamData[0]['@Score'] : 0;
                $ScoreAway = isset($TeamData[1]['@Score']) ? $TeamData[1]['@Score'] : 0;

                $TotalGoals = $ScoreHome + $ScoreAway;

                $MatchWinner = isset($MatchInfo['@MatchWinner']) ? $MatchInfo['@MatchWinner'] : null;


                $Match = Match::where('id', '=', $matchDatum['@uID'])->first();

                if (!$Match) {
                    Match::create([
                        'id' => $matchDatum['@uID'],
                        'competition_id' => $this->Competition->id,
                        'match_winner' => $MatchWinner,
                        'home_team' => $HomeTeam,
                        'away_team' => $AwayTeam,
                        'date' => $MatchInfo['Date'],
                        'total_goals' => $TotalGoals,
                        'ended' => !is_null($MatchWinner) ? 1 : 0
                    ]);
                }
                $this->Match[] = $matchDatum['@uID'];
            }

        } catch (\Exception $e) {
            dd($e->getMessage());
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMatchData()
    {
        return $this->matchData;
    }

    /**
     * @param array $value
     * @return $this
     */
    public function setTimingType(array $value)
    {
        $this->timingType = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTimingType()
    {
        return $this->timingType;
    }


    public function run()
    {

        $this->parse()
            ->setup();

//        exit;
//
//        try {
//            # Instância o Parser
//            $parser = new Parser();
//            # Lê o XML
////            $parsed = $parser->xml(file_get_contents(storage_path("srml-5-2013-results-group-competition-.xml")));
//            $parsed = $parser->xml(file_get_contents(storage_path("carioca-1.xml")));
//            # Lê o nó SoccerDocumento
//            $parsed = $parsed['SoccerDocument'];
//
//            dd($parsed);
//
//            $competition_name = $parsed['@competition_name'];
//            $competition_id = $parsed['@competition_id'];
//            $season_id = $parsed['@season_id'];
//
//            # Procura a competição pelo ID
//            $competition = app($this->Competition)->find($competition_id);
//            if (!$competition) {
//                app($this->Competition)->create(["id" => $competition_id, "name" => $competition_name, "season_id" => $season_id]);
//            }
//
//            # Procura pelos times, para não repetir a inserção
//            $existTeams = app($this->Team)->pluck("code", "id");
//            foreach ($parsed["Team"] as $team) {
//                if (!$existTeams->search($team['@uID'])) {
//                    $t = app($this->Team)->create([
//                        "code" => $team['@uID'],
//                        "name" => $team['Name'],
//                        "slug" => str_slug($team["Name"])
//                    ]);
//                    # Verifico se o time já não está vinculado a competição
//                    $competitionTeamCheck = app($this->CompetitionTeam)->where(["team_id" => $t->id, "competition_id" => $competition_id])->count();
//                    if (!$competitionTeamCheck) {
//                        # Vinculo o time com a competição
//                        app($this->CompetitionTeam)->create(["team_id" => $t->id, "competition_id" => $competition_id]);
//                    }
//                }
//            }
//        } catch (\Exception $e) {
//            dd($e->getMessage());
//        }
    }

}