<?php

namespace App\OptaPerform;

use App\Competition;
use App\CompetitionTeam;
use App\Season;
use App\Team;
use Nathanmac\Utilities\Parser\Parser;

trait ParseTrait
{

    private $soccerDocument;

    private $Competition;

    private $team;

    private $file;

    private $xml;

    /**
     * @param $value
     * @return $this
     */
    public function setSoccerDocument(array $value): self
    {
        $this->soccerDocument = $value;
        return $this;
    }

    /**
     * @param null $key
     * @return mixed
     */
    public function getSoccerDocument($key = null)
    {
        return !is_null($key) ? (isset($this->soccerDocument[$key]) ? $this->soccerDocument[$key] : []) : $this->soccerDocument;
    }

    /**
     * @param int $value
     * @param string $name
     * @return $this
     */
    public function setSeason(int $value, string $name)
    {
        $Season = new Season();
        $Exist = $Season->find($value);
        if (!$Exist) {
            $Season->create([
               'id' => $value,
               'name' => $name
            ]);
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function setCompetition()
    {
        $Competition = new Competition();

        $competition_code = $this->getSoccerDocument('@competition_code');
        $competition_name = $this->getSoccerDocument('@competition_name');
        $competition_id = $this->getSoccerDocument('@competition_id');
        $season_id = $this->getSoccerDocument('@season_id');

        # Cria a temporada, caso não exista
        $this->setSeason($season_id, $this->getSoccerDocument('@season_name'));

        $query = $Competition->where("id", "=", $competition_id)->first();

        if (!$query) {
            $Competition->create([
                "id" => $competition_id,
                "name" => $competition_name,
                "season_id" => $season_id,
                "code" => $competition_code
            ]);
        }

        $this->Competition = $Competition->where("id", "=", $competition_id)->first();
        return $this;
    }

    /**
     * @param null $key
     * @return mixed
     */
    public function getCompetition($key = null)
    {
        return !is_null($key) ? (isset($this->Competition[$key]) ? $this->Competition[$key] : []) : $this->Competition;
    }

    /**
     * @param string $value
     * @return self
     *
     * Arquivo será lido para inserção dos dados, adiciona o caminho completo /c/storage/ ou /c/arquivos e por aí vai...
     */
    public function setFile(string $value): self
    {
        $this->file = file_get_contents($value);
        return $this;
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * @return $this
     */
    public function parse(): self
    {
        try {
            $parser = new Parser();
            $this->xml = $parser->xml($this->getFile());
        } catch (\Exception $e) {
            $this->messageBag->add($e->getCode(), $e->getMessage());
        }
        return $this;
    }

    /**
     * @param null $key
     * @return mixed
     */
    public function getParse($key = null)
    {
        return !is_null($key) ? (isset($this->xml[$key]) ? $this->xml[$key] : []) : $this->xml;
    }

    /**
     * @param array $value
     * @return $this
     */
    public function setTeam(array $value)
    {
        $this->team = $value;

        if ($this->Competition instanceof Competition) {

            $Team = new Team();
            $CompetitionTeam = new CompetitionTeam();

            # Procura pelos times, para não repetir a inserção
            $existTeams = $Team->pluck("code", "id");
            foreach ($this->team as $team) {
                if (!$existTeams->search($team['@uID'])) {
                    $t = $Team->create([
                        "code" => $team['@uID'],
                        "name" => $team['Name'],
                        "slug" => str_slug($team["Name"])
                    ]);
                    # Verifico se o time já não está vinculado a competição
                    $competitionTeamCheck = $CompetitionTeam->where(["team_id" => $team['@uID'], "competition_id" => $this->Competition->id])->count();
                    if (!$competitionTeamCheck) {
                        # Vinculo o time com a competição
                        $CompetitionTeam->create(["team_id" => $t->id, "competition_id" => $this->Competition->id]);
                    }
                }
            }

        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTeam()
    {
        return $this->team;
    }

}