<?php

namespace App;



class PlayerStat extends BaseModel
{

    protected $fillable = ["player_id", "key", "value"];

}
