<?php

namespace App;

use App\Traits\JsonTrait;


class Player extends BaseModel
{

    use JsonTrait;

    protected $hidden = ['id', 'created_at', 'updated_at', 'team_id'];

    protected $fillable = ["team_id", "code", "name", "position"];

    public function About()
    {
        return $this->hasMany(PlayerStat::class, 'player_id', 'code');
    }

    public function Substitute()
    {
        return $this->belongsTo(Substitution::class, 'player_on', 'code');
    }

}
