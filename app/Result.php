<?php

namespace App;

use App\Traits\ModelTrait;


class Result extends BaseModel
{

    use ModelTrait;

    protected $fillable = [
        "parent_id", "code", "game_id", "stadium_id",
        "status", "status_match"
    ];

}
