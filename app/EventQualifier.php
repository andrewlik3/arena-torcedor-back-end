<?php

namespace App;



class EventQualifier extends BaseModel
{

    protected $fillable = ["code", "qualifier_id", "event_id", "value"];

}
