<?php

namespace App;



class GamePlayerLine extends BaseModel
{

    protected $hidden = ['created_at', 'updated_at', 'id', 'game_id', 'player_id'];

    protected $fillable = [
        "game_id", "player_id", "shirt_number", "position", "status"
    ];

    protected $with = ['Data', 'Change', 'Cards'];

    public function Change()
    {
        return $this->belongsTo(Substitution::class, 'player_id', 'player_on')->with(['PlayerOn', 'PlayerOff']);
    }

    public function Cards()
    {
        return $this->hasMany(GamePlayerBook::class, 'player_id', 'player_id');
    }

    public function Data()
    {
        return $this->belongsTo(Player::class, 'player_id', 'code')->select(['code', 'name', 'position']);
    }

    public function Stats()
    {
        return $this->hasMany(GamePlayerStat::class, 'game_player_line_id', 'player_id')->select(['game_player_line_id', 'key', 'value']);
    }

}
