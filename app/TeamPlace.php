<?php

namespace App;

use App\Traits\ModelTrait;


class TeamPlace extends BaseModel
{

    use ModelTrait;

    protected $fillable = ["team_id", "code", "capacity", "name"];

}
