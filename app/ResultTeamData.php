<?php

namespace App;



class ResultTeamData extends BaseModel
{

    protected $fillable = [
        "result_id", "team_id", "score", "shoot_out_score",
        "side"
    ];

}
