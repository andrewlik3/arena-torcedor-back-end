<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamKitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_kits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('team_id');
            $table->string('code');
            $table->string('color_one');
            $table->string('color_two')->nullable();
            $table->string('color_tree')->nullable();
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_kits');
    }
}
