<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultTeamDataGoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_team_data_goals', function (Blueprint $table) {
            $table->increments('id');

            $table->string('code');

            $table->string('type');

            $table->integer('team_data_id');

            $table->integer('event_id');
            $table->integer('event_number');

            $table->integer('minute');
            $table->integer('second');

            $table->integer('player_id');
            $table->integer('player_assist_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_team_data_goals');
    }
}
