<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');

            $table->string('type')->nullable();

            $table->string('period')->nullable();
            $table->string('match_type')->nullable();

            $table->integer('code')->nullable();
            $table->integer('competition_id')->nullable();
            $table->integer('season_id')->nullable();
            $table->integer('away_team_id')->nullable();
            $table->integer('home_team_id')->nullable();
            $table->integer('match_day')->nullable();
            $table->integer('first_match_id')->nullable();
            $table->integer('next_match_id')->nullable();
            $table->integer('match_winner')->nullable();
            $table->integer('score_home')->nullable();
            $table->integer('score_away')->nullable();
            $table->integer('total_goals')->nullable();

            $table->tinyInteger('ended')->default(0);

            $table->timestamp('date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
