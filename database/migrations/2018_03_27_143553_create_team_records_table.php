<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('competition_id');
            $table->string('team_id');
            $table->integer('points')->default(0);
            $table->integer('game_played')->default(0);
            $table->integer('win')->default(0);
            $table->integer('drawn')->default(0);
            $table->integer('loose')->default(0);
            $table->integer('gp')->default(0);
            $table->integer('gc')->default(0);
            $table->integer('sg')->default(0);
            $table->integer('position')->nullable();
            $table->integer('start_day_position')->nullable();
            $table->integer('current_round')->nullable();
            $table->longText('standing')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_records');
    }
}
