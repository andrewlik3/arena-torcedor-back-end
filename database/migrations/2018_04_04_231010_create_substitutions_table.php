<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubstitutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('substitutions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');

            $table->integer('game_id');
            $table->integer('player_on');
            $table->integer('player_off');

            $table->string('reason')->nullable();

            $table->integer('period')->default(1);
            $table->integer('minute')->default(0);
            $table->integer('second')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('substitutions');
    }
}
