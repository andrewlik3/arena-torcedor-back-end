<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamePlayerBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_player_books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->integer('player_id');
            $table->string('card_type')->nullable();
            $table->string('reason');
            $table->string('period');
            $table->integer('min')->default(0);
            $table->integer('sec')->default(0);
            $table->integer('time')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_player_books');
    }
}
