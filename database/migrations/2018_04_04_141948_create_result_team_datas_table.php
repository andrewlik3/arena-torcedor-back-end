<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultTeamDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_team_datas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('result_id');
            $table->integer('team_id');
            $table->integer('score')->default(0);
            $table->integer('shoot_out_score')->default(0);

            $table->string('side');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_team_datas');
    }
}
