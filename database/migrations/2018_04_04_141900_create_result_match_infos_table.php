<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultMatchInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_match_infos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('result_id');

            $table->string('period')->nullable();
            $table->string('match_type')->nullable();

            $table->string('result_type')->nullable();
            $table->integer('winner_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_match_infos');
    }
}
