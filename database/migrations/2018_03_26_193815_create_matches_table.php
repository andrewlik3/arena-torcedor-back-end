<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');

            $table->string('code');
            $table->string('competition_id');

            $table->string('first_match_id')->nullable();

            $table->string('next_match_id')->nullable();

            $table->string('match_winner')->nullable();
            $table->string('home_team');
            $table->integer('score_home')->default(0);
            $table->string('away_team');
            $table->integer('score_away')->default(0);

            $table->dateTime('date');

            $table->integer('total_goals')->default(0);

            $table->tinyInteger('ended')->default(0);

            $table->string('period')->nullable();
            $table->string('match_type')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
