<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('game_id');
            $table->string('code');
            $table->text('message');
            $table->string('minute', 11)->nullable();
            $table->string('period', 11)->nullable();
            $table->string('second', 11)->nullable();
            $table->string('time')->nullable();
            $table->string('type')->nullable();
            $table->dateTime('modified');
            $table->index(['game_id', 'code']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comment_messages');
    }
}
