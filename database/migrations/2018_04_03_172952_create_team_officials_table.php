<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamOfficialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_officials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->integer('team_id')->nullable();
            $table->string('name');
            $table->string('type')->nullable();
            $table->string('last_name')->nullable();
            $table->string('country')->nullable();
            $table->date('birth_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_officials');
    }
}
